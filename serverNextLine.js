const express = require('express')
const next = require('next')

var sendLine = require('./shared/sendLine')
//import { sendText } from '../shared/sendLine';

var bodyParser = require('body-parser')
var request = require('request')
var cors = require('cors');
//var app = express()

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare()
.then(() => {
  const server = express();

  server.use(cors());
  server.use(bodyParser.json())
  server.use(bodyParser.urlencoded({
    extended: true
  }))
  server.use(bodyParser.json())
  

  server.get('/p/:id', (req, res) => {
    const actualPage = '/post'
    const queryParams = { title: req.params.id }
    app.render(req, res, actualPage, queryParams)
  })

  server.get('/send',function(req,res){
    console.log('start sending...');

    var text = {
        type: 'text',
        text: 'Aylon was here Today'
      };
      var sender = 'U264ab1e2b57d6adcff784c0820285590'; // 'mcu9345p'; //'1588104142' // '21351379';
      sendLine.sendText(sender, text);
    });

  server.get('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(50000, (err) => {
    if (err) throw err
    console.log('> Ready on http://localhost:50000')
  })
})
.catch((ex) => {
  console.error(ex.stack)
  process.exit(1)
})