import axios from 'axios';
const mailInstance = axios.create({
    baseURL: 'http://localhost:3001'
});

export default mailInstance;