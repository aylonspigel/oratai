import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from '../axios-firebase';
import withErrorHandler from '../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../store/actions/index';
import Modal from './UI/Modal/Modal';
import Spinner from './UI/spinner/spinner';
import Add2AllForm from './add2AllForm';
import EditProdForm from './editProdForm';
import ProductDisplay from './productDisplay';
import Auxiliry from '../hoc/Auxiliry/Auxiliry';
import * as productGroupData from '../shared/productGroup.json';

class BasicProducts extends Component {
    constructor(props) {
        super(props);
        // this.changeFilter = this.changeFilter.bind(this);
    }
    // product type should be moved out
    state = {
        proShow: false,
        ProductsByGroup: null,
        loading: false,
        currentGroupId: 'all',
        pin: null
    }

    componentDidMount() {
        // AJAX CALL
        this.props.onFetchAllProducts();
   }
    
  
    componentWillReceiveProps(nextProps, nextState) {
        if(nextProps.allProducts !== this.props.allProducts){
           let filteredProductArr = null; //nextProps.allProducts;
            if (this.state.currentGroupId === 'all') {
                filteredProductArr = []; // for OLD all product button;
            }
            else { filteredProductArr = nextProps.allProducts.filter(product => product.group_id === this.state.currentGroupId); 
            }

            this.setState(
                {
                    ProductsByGroup: [...filteredProductArr]
            })
            
        }
    }

    OpenMenu = (type_id) => {
        let filteredProductArr = null; // this.props.allProducts;
        if (type_id === 'all') {
            filteredProductArr = null; // this.props.allProducts;
        }
        else { filteredProductArr = this.props.allProducts.filter(product => product.group_id === type_id); 
        }

        this.setState(
            {
                ProductsByGroup: [...filteredProductArr],
                currentGroupId: type_id
        })
    }        

    handleAdd = () => {
        this.props.onAdd2AllOpen();
    }

    openProductModal = (pin) => {
        //this.props.onOpenProduct();
        let currentProductId=this.state.ProductsByGroup[pin].id;
        console.log('currentProductId',currentProductId);
        this.props.onUpdateCurrentProductId(currentProductId)

        this.setState({p_in: pin, proShow: true});
    }

    closeProductPage = () => {
        //this.props.onOpenProduct();
        this.setState({proShow: false});
    }

    handleEditClose = () => {
        this.props.onCloseEdit();
        this.props.onFetchAllProducts();
        this.closeProductPage();
    }

    render() {
        let AdminModals;
        if(this.props.isAdmin===true){
            AdminModals = (
           <>
           <Modal name="add2AllModal" 
                show={this.props.add2AllShow} 
                modalClosed={this.props.onAdd2AllClose} >
               <button className="btn btn-link" 
               onClick={this.props.onAdd2AllClose}>X</button>
               <Add2AllForm  />
           </Modal >
           </>
       );
       }else{
           AdminModals = '';
       }

       let addButton =   ''; 
     if(this.props.isAdmin===true){
         addButton = <div id="add-button" key={'addproduct'} className="productMenuButton"  
            onClick={() => this.handleAdd()} >
            <img className="add-button-img" src={'../static/add.png'} alt="Thai Sarong" />
            <br/>
            <p>Add New Product</p>
        </div>;
        } 
    let productGroup = () => {
        let productMap= null;
        if(!this.props.loading && this.state.ProductsByGroup){
            productMap = this.state.ProductsByGroup.map((product, index) => (
                <div key={'product'+index} className="productMenuButton"  
                onClick={() => this.openProductModal(index)}>
                    <span>{product.title}</span>
                    <img src={'../static/'+product.photo_url} alt={`'Thai Sarong-'+product.title`} />
                </div>
                ));
         }
           return <Auxiliry>
               {productMap}
            <style jsx global>{`

.productMenuButton{
    display: flex;
    flex-wrap: wrap;
    position: relative;
    align-items: center;
    justify-content: center;
    width: 16vw;
    height: 16vw;
    border: 2px solid white;
    border-radius: 10px;
    cursor: pointer;
    margin: 10px;
    overflow: hidden;
}



.productMenuButton > span{
    background-color: black; // #3d2115;
    width: 100%;
    text-align: center;
    z-index: 2;
    position: relative;
    font-size: 12px;
    padding: 5px;
    color: white;
    bottom: -50px;
}
.productMenuButton > img {
    position: absolute;
    left: 0;
    top: 0;
    width: 16vw;
    height: auto;
    z-index: 1;
    object-fit: cover;
}

#add-button > .add-button-img{
    position: relative;
    width: 70%;
    height: 70%;
    margin: 0 auto;
}
#add-button > p{
    color: white;
    font-size: 18px;
}

.productMenuButton:hover, 
.productMenuButton:focus{
    border-radius: 20px!important;
}
            `}</style>
                </Auxiliry>;
    }
        
        
        // let product = this.props.productsInfo[this.state.current_product_index];
        
        

        let thePage = () => {
           
           // if(this.props.proShow){
            if(this.state.proShow){
                return <Auxiliry>
                   {(this.props.isAdmin===true)?<Modal   name="editProdModal" 
                        show={this.props.editShow} 
                        modalClosed={this.props.onCloseEdit}
                        >
                        <button className="btn btn-link" 
                        onClick={this.props.onCloseEdit}>X</button>
                        <EditProdForm  editModalClose={this.handleEditClose}  />
                    </Modal >:null}

                    <ProductDisplay   
                        closeShow={this.closeProductPage}
                        pin={this.state.p_in}
                        productsInfo={(this.state.ProductsByGroup)?this.state.ProductsByGroup:null}
                    />;
                </Auxiliry>
            }else{
                return <Auxiliry>
                {AdminModals}
                <div className="menu-box">
                    {/* <a className="allproducts menu-cat-all" valign="center"
                        onClick={() => this.OpenMenu('all')} >
                        <span>All Products</span>
                        
                    </a> */}
                    <ul className="category-list">
                    {productGroupData.productGroup.map(category => (
                        <li className={(this.state.currentGroupId===(category.id).toString())?'category-btn activeted':'category-btn'} key={'cat' + (category.id).toString()} >
                        <a className="menu-cat"  onClick={() => this.OpenMenu((category.id).toString())}
                            ><span>{category.title}</span>
                            <img src={category.url} alt={category.title}  />
                            </a>
                        </li>
                    ))}
                    </ul>
                </div>
                <div className="product-group">
                    {productGroup()}
                    {addButton}
                </div>
                <style jsx global>{`

                .menu-box{
                    margin: 0 auto 10px auto;
                    padding-bottom: 10px;
                    width: 100%;
                    max-width: 1000px;
                    text-align: center;
                    display: flex;
                    justify-content: space-evenly;
                    flex-wrap: nowrap;
                    border-bottom: 9px double white;
                }
           
                .category-list{
                    order: 2;
                    width: 100%;
                    list-style-type: none;
                    margin: 0;
                    padding: 0;
                }
                .category-btn{
                    display: flex;
                    float: left;
                    width: 180px;
                    height: 180px;
                    border: 2px solid white;
                    border-collapse: separate;
                    background-color: black;
                    position: relative;
                    overflow: hidden;
                    margin: 10px;
                    padding: 0px;
                }

                
                .product-group{
                    display: flex;
                    flex-wrap: wrap;
                    justify-content: space-evenly;
                    width: 100%;
                    min-height: 230px;
                    max-width: 1000px;
                    margin: 0 auto;
                }
                                
                .menu-cat{
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    width: 100%;
                    height: 100%;
                    text-decoration: none;
                    vertical-align: middle;
                    color: white;
                }
                .category-list > .category-btn > .menu-cat > span{
                    text-align: center;
                    z-index: 2;
                    position: relative;
                    color: #fff;
                    
                    font-weight: 700;
                    padding: 3px;
                    background-color: #221111;
                    width: 100%;
                    height: 50px;
                    top: 65px;
                    
                }
                .category-list > .category-btn > .menu-cat:hover > span{
                    color: yellow;
                }
                .category-list > .category-btn > .menu-cat > img{
                    position: absolute;
                    left: 0;
                    top: 0;
                    width: 180px;
                    height: 180px;
                    z-index: 1;
                    opacity: 1;
                    object-fit: cover;
                }

                .category-list > .category-btn:hover, 
                .category-list > .category-btn:focus,
                .category-list > .category-btn.activeted{
                    border-radius: 20px!important;
                }
                .menu-cat:hover,  .menu-cat-all:hover, 
                .menu-cat:focus,  .menu-cat-all:focus{
                    cursor: pointer;
                    color: white!important;
                    text-decoration: none;
                    border-radius: 20px!important;
                }
                
    @media screen and (min-width: 1401px) {
        
                    .category-btn{
                        width: 180px;
                        height: 180px;
                    }

                    .category-list > .category-btn > .menu-cat > img{
                        width: 180px;
                    }

                        div.menu-box > ul.category-list > li.category-btn > 
                        a.menu-cat > span{
                            font-size: 14px;
                            top: 65px;

                            background-color: #221111;
                            width: 100%;
                            height: 50px;
                        }

                        .productMenuButton > span{
                            font-size: 16px;
                            bottom: -70px;
                        }

                        .productMenuButton{
                            border-radius: 6px;
                            width: 200px;
                            height: 200px;
                            margin: 9px;
                        }
                        .productMenuButton > img {
                            width: 220px;
                            object-fit: cover;
                        }
                }
    @media screen and (min-width: 1020px) and (max-width: 1400px) {

        .category-btn{
            width: 180px;
            height: 180px;
        }

        .category-list > .category-btn > .menu-cat > img{
            width: 180px;
        }

        div.menu-box > ul.category-list > li.category-btn > 
                    a.menu-cat > span{
                        font-size: 14px;
                        top: 65px;
                        
                    }

                    .productMenuButton > span{
                        font-size: 16px;
                        bottom: -75px;
                    }

                    .productMenuButton{
                        border-radius: 6px;
                        width: 180px;
                        height: 180px;
                        margin: 9px;
                    }

                    .productMenuButton > img {
                        width: 180px;
                        object-fit: cover;
                    }
                }
    @media screen and (min-width: 805px) and (max-width: 1019px) {
                    .menu-box{
                            width: 100%;
                            flex-wrap: wrap;
                            padding: 2%;
                        }

                        
                        
                    .menu-box > .category-list{
                            order: 2;
                            width: 100%;
                            display: flex;
                            flex-wrap: wrap;
                            text-align: center;
                            justify-content: center;
                        }
                     
                    .category-btn{
                            width: 16%;
                            height: 140px;
                        }

                        .category-list > .category-btn > .menu-cat > img{
                            width: 150px;
                            height: 150px;
                            object-fit: cover;
                        }

                        div.menu-box > ul.category-list > li.category-btn > 
                        a.menu-cat > span{
                            font-size: 14px;
                            top: 65px;
                            background-color: #221111;
                            width: 100%;
                            height: 50px;
                        }
                        

                        .productMenuButton{
                            border-radius: 6px;
                            width: 180px;
                            height: 180px;
                            margin: 9px auto;
                            
                        }
                        .productMenuButton > img {
                            width: 180px;
                            object-fit: cover;
                        }
                        .productMenuButton > span{
                            width: 100%;
                            font-size: 16px;
                            top: 75px;
                        }
                    }

        @media screen and (min-width: 451px) and (max-width: 805px) {
                        .menu-box{
                            width: 100%;
                            flex-wrap: wrap;
                        }
                       
                        .menu-box > .category-list{
                            order: 2;
                            width: 100%;
                        }
                        
                        div.menu-box > ul.category-list > li.category-btn > 
                        a.menu-cat > span{
                            font-size: 16px;
                            top: 35px;
                            background-color: #221111;
                            width: 100%;
                            height: 30px;
                        }
                        .category-btn{
                            width: 46%;
                            height: 100px;
                            margin: 6px;
    
                        }
                        .category-list > .category-btn > .menu-cat > img{
                            width: 46vw;
                            object-fit: cover;
                        }

                        .productMenuButton > span{
                            width: 100%;
                            top: 60px;
                            height: 30px;
                        }
                        .productMenuButton{
                            border-radius: 6px;
                            width: 150px;
                            height: 150px;
                            margin: 5px;
                            
                        }
                        .productMenuButton > img {
                            width: 150px;
                            object-fit: cover;
                        }
                    }

    @media screen and (max-width: 450px) {
                        div.menu-box > ul.category-list > li.category-btn > 
                        a.menu-cat > span{
                            font-size: 14px;
                            top: 35px;
                            background-color: #221111;
                            width: 100%;
                            height: 30px;
                        }

                        .menu-box{
                            width: 100%;
                            flex-wrap: wrap;
                        }
                        .menu-box > .allproducts{
                            order: 1;
                            width: 100%;
                            height: 60px;
                            margin: 2%;
                        }
                        .menu-box > .category-list{
                            order: 2;
                            width: 100%;
                        }
                        
                        .category-list > .category-btn:hover, 
                        .category-list > .category-btn:focus,
                        .category-list > .category-btn.activeted{
                            border-radius: 10px!important;
                        }
                        
                        .category-btn{
                            width: 46%;
                            height: 100px;
                            margin: 6px;
    
                        }
                        .category-list > .category-btn > .menu-cat > img{
                            width: 46vw;
                            object-fit: cover;
                        }

                        .productMenuButton > span{
                            width: 100%;
                            top: 60px;
                        }
                        .productMenuButton{
                            border-radius: 6px;
                            width: 150px;
                            height: 150px;
                            margin: 5px;
                            
                        }
                        .productMenuButton > img {
                            width: 150px;
                            object-fit: cover;
                        }

                        #add-button > p{
                            font-size: 12px;
                        }
                    }

                `}</style>
                </Auxiliry>;
            }
        }

        return <Auxiliry> {thePage()} </Auxiliry>;
    }
}

const mapStateToProps = state => {
    return {
        isAdmin: state.auth.isAdmin,
        allProducts: state.allProducts.allProducts,
        productAdded: state.allProducts.productAdded,
        loading: state.allProducts.loading,
        add2AllShow: state.allProducts.add2AllShow,
        proShow: state.allProducts.proShow,
        editShow: state.allProducts.editShow,
        productId: state.allProducts.currentProductId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchAllProducts: () => dispatch( actions.fetchAllProducts()),
        onAdd2AllOpen:      () => dispatch( actions.add2AllOpen()),
        onAdd2AllClose:     () => dispatch( actions.add2AllClose()),
        onOpenProduct:      () => dispatch( actions.openProduct()),
        onCloseProduct:     () => dispatch( actions.closeProduct()),
        onCloseEdit:        () => dispatch( actions.closeEdit()),
        onUpdateCurrentProductId: (id) => dispatch( actions.updateCurrentProductId(id)),
    };
};

export default connect( mapStateToProps, mapDispatchToProps )( withErrorHandler( BasicProducts, axios ) );