import React, { Component } from 'react';
import PropTypes from "prop-types";

import Thumb from "../UI/Thumb";
import util from '../../shared/util';


class CartProduct extends Component {

  state = {  isMouseOver: false, }

  handleMouseOver = () => {
    this.setState({isMouseOver: true});
  }

  handleMouseOut = () => {
    this.setState({isMouseOver: false});
  }


  render(){
    const { product, removeProductCart } = this.props;

    const classes = ['shelf-item'];

    if(!!this.state.isMouseOver){
      classes.push('shelf-item--mouseover');
    }

    // src={`../../static/products/${product.sku}_2.jpg`}
    return (
      <div className={classes.join(" ")}>
        <div
          className="shelf-item__del"
          onMouseOver={() => this.handleMouseOver()}
          onMouseOut={() => this.handleMouseOut()}
          onClick={() => removeProductCart(product)}
        />
        <Thumb
          classes="shelf-item__thumb"

          src={`../../static/${product.photo_url}`}
          alt={product.product_name}
        />
        <div className="shelf-item__details">
          <span className="name">{product.product_name}</span><br/>
          <span className="pattern">Pattern ID: {product.pattern_id}</span><br/>
          <span className="pattern">Color: {product.color}</span><br/>
          <span className="desc">Desc: {product.description}</span><br/>
          <span>
            Quantity: {product.qty}
          </span>
        </div>
        <div className="shelf-item__price">
          <p>{`${product.currencyFormat}  ${util.formatPrice(product.retail_price)}`}</p>
        </div>

        <div className="clearfix" />
      </div>
    );
  }
}


CartProduct.propTypes = {
  product: PropTypes.object.isRequired,
  removeProduct: PropTypes.func.isRequired,
};

export default CartProduct;