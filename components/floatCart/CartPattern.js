import React, { Component } from 'react';
import PropTypes from "prop-types";

import Thumb from "../UI/Thumb";
import util from '../../shared/util';


class CartPattern extends Component {

  state = {  isMouseOver: false, }

  handleMouseOver = () => {
    this.setState({isMouseOver: true});
  }

  handleMouseOut = () => {
    this.setState({isMouseOver: false});
  }


  render(){
    const { products, currencyFormat } = this.props;

    const colorsCluster = products.map( p => (
                            <div key={p.product_id} className="colorsCluster" >
                                {/* <Checkbox
                                    label={p.color}
                                    // handleCheckboxChange={this.basketChange} 
                                    key={p.color_id}
                                    value={p.color_id}
                                />  */}
                              {p.color},&nbsp;
                            </div>
                        )); 

    const classes = ['shelf-item'];

    if(!!this.state.isMouseOver){
      classes.push('shelf-item--mouseover');
    }

    // src={`../../static/products/${product.sku}_2.jpg`}
    return (
      <div className={classes.join(" ")}>
        <div
          className="shelf-item__del"
          onMouseOver={() => this.handleMouseOver()}
          onMouseOut={() => this.handleMouseOut()}
          onClick={() => removeProductCart(product)}
        />
        <Thumb
          classes="shelf-item__thumb"

          src={`../../static/${products[0].photo_url}`}
          alt={products[0].product_name}
        />
        <div className="shelf-item__details">
          <span className="pattern">Pattern:&nbsp; {products[0].pattern_id}</span><br/>
          <span className="desc">Desc:&nbsp; {products[0].description}</span><br/>
            <div className="colorsClusterBox" >
          Colors:&nbsp; {colorsCluster}
          </div>
          <span>
            Quantity: {products.length}
          </span>
        </div>
        <div className="shelf-item__price">
          <p>{`${this.props.currencyFormat} ${util.formatPrice(products[0].retail_price)}`}</p>
        </div>

        <div className="clearfix" />
      </div>
    );
  }
}


CartPattern.propTypes = {
    products: PropTypes.array.isRequired,
};

export default CartPattern;