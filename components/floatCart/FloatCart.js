import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

import CartPattern from './CartPattern'
import UserDetail from '../UserDetail'

import util from '../../shared/util';
import axios2mail from '../../axios-mail';
import axios from 'axios';
import { sendText } from '../../shared/sendLine';
import request from 'request';



class FloatCart extends Component {
  constructor(props) {
    super(props);
    this.proceedToCheckout = this.proceedToCheckout.bind(this);
}


  state = {
    formValid: false,
    userDetails: null,
    showUserForm: true,
    isOpen: false,
    cartOrder: null
   };

  componentWillMount() {
  //  this.props.onLoadCart(  );
  //const cartOrder = this.props.cartOrder;
  }

  componentDidMount() {
    this.props.onLoadCart( );

    setTimeout(() => {
      this.props.onUpdateCart(this.props.cartProducts);
    }, 0);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.newProduct !== this.props.newProduct) {
     // this.addProductCart(nextProps.newProduct);
    }
    if (nextProps.userDetails !== this.props.userDetails) {
      this.setState({userDetails:nextProps.userDetails});
    }
  }

  openFloatCart = () => {
    this.setState({ isOpen: true });
  }

  closeFloatCart = () => {
    this.setState({ isOpen: false });
  }

  handleLogin = () => {
    //close cart
    this.closeFloatCart();
    //open Login
    this.props.onAuthOpen();
}


  proceedToCheckout = (formValid) => {
    this.setState({formValid: formValid});
    const cartOrder = this.props.cartOrder;
    let userDetails = (localStorage.getItem("userDetails")?localStorage.getItem("userDetails"):null);
    const userId = localStorage.getItem('userId');

   // if(userId && !(JSON.stringify(userDetails).includes(userId.toString()))){
      if(userId && !(userDetails.toString().includes(userId.toString()))){  
      localStorage.setItem("userDetails",null);
      userDetails=null;
    }
    //const userDetails = this.props.userDetails;
    //const userDetails2 = this.state.userDetails;
    const cartTotals = cartOrder.cartTotals;
   // console.log('userDetails', this.props.userDetails);
    
    if(userId && (userDetails.toString().includes(userId.toString()))
                    && this.props.isAuthenticated && cartTotals.total_qty>0 ){
        //console.log('user2', userDetails);
      //  this.props.onCart2Order(cartOrder,userDetails);
      //  this.setState({cartOrder:null});
        // send email
        const subject = `UserId ${userId} Order`;
        const utf8Subject = `=?utf-8?B?${Buffer.from(subject).toString('base64')}?=`;

        const email = JSON.parse(userDetails).email;
        const username = JSON.parse(userDetails).name;
        const phone = JSON.parse(userDetails).phone;
      //  console.log('email',email);
      //  console.log('username',username);
        const messageParts = [
          `${subject} `,
         `${email}`,
        `${username}`,
        `${phone}`
        ]
        cartOrder.cartProducts.map((product, index) => {
          let n = index + 1;
          messageParts.push(`${n}. Pattern: ${product.pattern_id} - ${product.color} - ${product.qty}`);
        });
        let cf = "฿" // cartOrder.cartTotals.currencyFormat

        messageParts.push(`Total of ${cartOrder.cartTotals.total_qty} items. `);
        messageParts.push(`Subtotal: ${cf} ${util.formatPrice(cartOrder.cartTotals.sub_total, cartOrder.cartTotals.currencyId)}`);
        messageParts.push(`Shipping: ${cf} ${util.formatPrice(cartOrder.cartTotals.shipping, cartOrder.cartTotals.currencyId)}`);
        messageParts.push(`Tax: ${cf} ${util.formatPrice(cartOrder.cartTotals.tax, cartOrder.cartTotals.currencyId)}`);
        messageParts.push(`Total Price:${cf} ${util.formatPrice(cartOrder.cartTotals.totalPrice, cartOrder.cartTotals.currencyId)}`);

        const message = messageParts.join("\n");
      //  console.log('message:',message);
        const to = 'alonspigel@yahoo.com';
        
       const message1 = {
        type: 'text',
        text: message.toString()
      };
    
 var message2=message.toString();
var uri = `https://orataiphathai.com/send?message1=${message2}`; 
//var uri = `https://testthisnow.ga/send?message1=${message2}`;
var uriEncoded = encodeURI(uri);
//console.log('send line uriEncoded',uriEncoded);
  const m4line = axios.create({
        //baseURL: 'http://localhost:50001/' 
    });
    m4line.get(uriEncoded)
          .then(response => {
            //console.log('action sendmail Success',response);
        })
        .catch(error => {
            //console.log('axios sendmail error' + error);
        }); 


        this.props.onCart2Order(cartOrder,userDetails);
        
      //clearing the form and closing the cart
      this.setState({formValid: false, cartOrder: null});

      this.closeFloatCart();
      
      }else{ //console.log('userDetails donot includes userId', userDetails);
      this.setState({showUserForm:true});
    //  console.log('user1', userDetails);
      //alert("Please fill your user details!");
    }
  }

 

  render() {
    const { removeProductCart } = this.props;
    const cartOrder = this.props.cartOrder;
//console.log('this.props.cartOrder',this.props.cartOrder);
//console.log('cartOrder',cartOrder);
    let products=null;

    if(!cartOrder){
      products = (<p style={{'textAlign': 'center'}}>Cart is Empty</p>)
    }else{
      products = cartOrder.cartPatterns.map(pattern => {
        let patternCluster = cartOrder.cartProducts.filter(p => p.pattern_id === pattern)
      
      return   (
        <CartPattern
          products={patternCluster}
          currencyFormat={cartOrder.cartTotals.currencyFormat}
          key={pattern}
        />
      );
    });
  }  
  
  
    let classes = ['float-cart'];
    if (!!this.state.isOpen) {
      classes.push('float-cart--open');
    }

    return (
      <div className={classes.join(' ')}>
        {/* If cart open, show close (x) button */}
        {this.state.isOpen && ( <div onClick={() => this.closeFloatCart()}
            className="float-cart__close-btn" > X </div>
        )}

        {/* If cart is closed, show bag with quantity of product and open cart action */}
        {!this.state.isOpen && (
          <span
            onClick={() => this.openFloatCart()}
            className="bag bag--float-cart-closed"
          >
            <span className="bag__quantity">{(cartOrder)?cartOrder.cartTotals.total_qty:0}</span>
          </span>
        )}

        <div className="float-cart__content">
          <div className="float-cart__header">
            
            <span className="header-title"> {this.state.isOpen && ((cartOrder)?cartOrder.cartTotals.total_qty:0)} items in your Basket!</span><br/>
            <span className="header-title"> 
            {this.state.isOpen && (
            (cartOrder)?cartOrder.cartPatterns.length:0)} types of patterns
            </span><br/>
            include the colors that you have chosen.
          </div>

          <div className="float-cart__shelf-container">
            {products}
            {products.length===0 && (
              <p className="shelf-empty">
                Add some product in the bag <br />:)
              </p>
            )}
          </div>

          <div className="float-cart__footer">
            {cartOrder && cartOrder.cartTotals && (
            <div className="sub-price">
              <p className="sub-price__val">
              Subtotal: {`${cartOrder.cartTotals.currencyFormat} ${util.formatPrice(cartOrder.cartTotals.sub_total, cartOrder.cartTotals.currencyId)}`}
              </p>
              <p>Shipping: {`${cartOrder.cartTotals.currencyFormat} ${util.formatPrice(cartOrder.cartTotals.shipping, cartOrder.cartTotals.currencyId)}`}</p>
              <p>Tax: {`${cartOrder.cartTotals.currencyFormat} ${util.formatPrice(cartOrder.cartTotals.tax, cartOrder.cartTotals.currencyId)}`}</p>
              <p className="total-price">Total Price: {`${cartOrder.cartTotals.currencyFormat} ${util.formatPrice(cartOrder.cartTotals.totalPrice, cartOrder.cartTotals.currencyId)}`}</p>
            </div>)}
            {/* <div onClick={() => this.proceedToCheckout()} disabled={!this.formValid}className="buy-btn"> Checkout   </div> */}
          </div>
    <div className="errMassages">
    {(!this.props.isAuthenticated)?(<span className="alert">1. First, please <button className="btn btn-default" onClick={() => this.handleLogin()}>CLICK</button> to login/signup!<br/></span>):<span className="green">1. First, you are logged in!<br/></span>}

    {(products.length>0)?
      (<span className="green">2. Great Choice of Products!<br/></span>):
      (<span className="alert">2. Please add some products to the basket!<br/></span>)
      }

    {((this.props.userDetails) || (this.state.formValid) )? 
      (<span className="green">3. We will use these details to contact you!<br/></span>):
      (<span className="alert">3. Last, please fill your user details!<br/>
                                We will contact you by email first.</span>)
  }
    </div>
     <UserDetail checkOut={this.proceedToCheckout } 
     formValid={this.state.formValid && (this.props.cartOrder !== null)} />
        </div>
        <style jsx >{`
        .errMassages{
          text-align: center;
        }
        .green{
          color: green;

        }
        .alert{
          color: red;
          font-weight: bold;
          text-align: center;
        }  
        `}</style>
      </div>
    );
  }
}

FloatCart.propTypes = {
  loadCart: PropTypes.func,
  updateCart: PropTypes.func,
  cartProducts: PropTypes.array,
  productsToAdd: PropTypes.array,
  removeProductCart: PropTypes.func,
  productsToRemove: PropTypes.array,
};

// productsToAdd: state.productsToAdd,
// productsToRemove: state.productsToRemove

  const mapStateToProps = state => {
    return {
      cartOrder: state.floatCart.cartOrder,
      userDetails: state.auth.userDetails,
      isAuthenticated: state.auth.token !== null,
      isAdmin: state.auth.isAdmin,
    };
  };

const mapDispatchToProps = dispatch => {
  return {
    onLoadCart: (cartOrder) =>  dispatch( actions.loadCart(cartOrder) ), 
    onUpdateCart: (cartProductsArr) =>  dispatch( actions.updateCart(cartProductsArr) ),
    removeProductCart: () =>  dispatch( actions.removeProductCart() ),
    // addProductCart: () =>  dispatch( actions.addProductCart() ),
    onCart2Order: (cartOrder, userDetails) => dispatch(actions.cart2Order(cartOrder, userDetails)),
    cartOpen: () =>  dispatch( actions.cartOpen() ),
    cartClose: () =>  dispatch( actions.cartClose() ),
    onAuthOpen: () => dispatch (actions.authOpen()),
  }
}

export default connect( mapStateToProps, mapDispatchToProps )( FloatCart );
