import React, { Component } from "react";
import Link from 'next/link';
import Router from 'next/router';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as actions from '../store/actions/index';

class Nav extends Component {
  //pathname =  'test5';
  constructor(props) {
    super(props);
    // this.changeFilter = this.changeFilter.bind(this);
}
state = {
  pathname: '/'
}

  
  componentDidMount(){
    //console.log('this.props. Router.pathname', Router.pathname); 
    this.setState({pathname: Router.pathname });
  }
 
  
  handleAdd = () => {
    // console.log('add open1');
    this.props.onAddOpen();
  }
  
  render() {
    const { pathname } = this.state;

    //console.log('this pathname', pathname);
    //console.log('this.pathname', this.state.pathname);


   let isAdmin = this.props.isAdmin;
//   console.log('isAdmin', isAdmin); // string is not 

/* no more orders online
    let orderButton = '';

    if(this.state.pathname === '/products') {
      orderButton = (
      <li key="orders"  className="nav-item">
        <a id="orders" className="nav-link" 
            onClick={this.props.onOrdersOpen}>
          <span className="glyphicon glyphicon-list menuicon"></span><br/>
          <span className="menutext">Orders</span>
        </a>
      </li>);
    }
    else{ orderButton = ''; } */

    let authlinks = '';
    //this.props.isAdmin
    if ( (this.props.isAuthenticated===true) && (isAdmin===true) ) {
      authlinks = (
      <ul className="nav navbar-nav navbar-right"  id="admin-bar">
        <li key="admin" className="nav-item">
              <span className="glyphicon glyphicon-wrench admin"></span><br/>
              <span className="menutext admin">Admin</span> 
          </li>
          {/* not add from here
          <li key="addP"  className="nav-item contact">
            <a id="addP" className="nav-link" 
                onClick={this.handleAdd} >
              <span className="glyphicon glyphicon-plus menuicon"></span><br/>
              <span className="menutext">Add New</span>
            </a>
          </li> */}
          {/* no more orders
           <li key="orders"  className="nav-item">
            <a id="orders" className="nav-link" 
                onClick={this.props.onOrdersOpen}>
              <span className="glyphicon glyphicon-list menuicon"></span><br/>
              <span className="menutext">Orders</span>
            </a>
          </li> */}
          <li key="logout" role="listitem" aria-label="logout" className="nav-item">
            <a  id="logout" className="nav-link" 
                onClick={this.props.onLogOut}>
              <span className="glyphicon glyphicon-log-out menuicon"></span><br/>
              <span className="menutext">Log Out</span>
            </a>
          </li>
        </ul>
      );
    }else if ( this.props.isAuthenticated===true ){
      authlinks = (
        <ul className="nav navbar-nav navbar-right"  id="auth-bar">
          
          <li key="logout" role="listitem" aria-label="logout" className="nav-item">
            <a  id="logout" className="nav-link" 
                onClick={this.props.onLogOut}>
              <span className="glyphicon glyphicon-log-out menuicon"></span><br/>
              <span className="menutext">Log Out</span>
            </a>
          </li>
        </ul>
      );
    } else {
      authlinks = (
        <ul className="nav navbar-nav navbar-right"  id="auth-bar">
          <li key="login" role="listitem" aria-label="login" className="nav-item">
            <a id="logout" className="nav-link" 
                onClick={this.props.onAuthOpen}>
              <span className="glyphicon glyphicon-user menuicon"></span><br/>
              <span className="menutext">Login</span>
            </a>
          </li>
        </ul>
      );
    }

      return (
  <nav className="navbar " id="main-navbar">
    <div className="container-fluid mynav">
      <ul name="navigation bar" role="navigation list" className="nav navbar-nav" id="mynavbar">
        <li name="home" role="listitem" aria-label="home" className="nav-item">
          <Link href='/'>
            <a className={pathname === '/' ? 'nav-link active' : 'nav-link'}>
              <span className="glyphicon glyphicon-home menuicon"></span><br/>
              <span className="menutext">Home</span>
            </a>
          </Link>
          </li>
          <li name="about_us" role="listitem" aria-label="about us" className="nav-item contact">
          <Link href='/about'>
            <a className={pathname === '/about' ? 'nav-link active' : 'nav-link'}>
              <span className="glyphicon glyphicon-question-sign menuicon2"></span><br/>
              <span className="menutext">About Us</span>
            </a>
          </Link>
          </li>
          <li name="products-catalog" role="listitem" aria-label="product catalog" className="nav-item">
          <Link href='/products' >
            <a className={pathname === '/products' ? 'nav-link active' : 'nav-link'}>
              <span className="glyphicon glyphicon-shopping-cart menuicon"></span><br/>
              <span className="menutext">Products</span>
            </a>
          </Link>  
          </li>
          {/* <li name="tips" role="listitem" aria-label="tips" className="nav-item">
          <Link href='/tips'>
            <a  className={pathname === '/tips' ? 'nav-link active' : 'nav-link'}>
            <span className="glyphicon glyphicon-info-sign menuicon"></span><br/>
              <span className="menutext">Tips</span>
            </a>
          </Link> 
          </li> */}
          <li name="reviews" role="listitem" aria-label="reviews" className="nav-item">
          <Link href='/reviews'>
            <a  className={pathname === '/reviews' ? 'nav-link active' : 'nav-link'}>
              <span className="glyphicon glyphicon-thumbs-up menuicon"></span><br/>
              <span className="menutext">Reviews</span>
            </a>
          </Link> 
          </li>
          <li name="customers" role="listitem" aria-label="customers" className="nav-item ">
          <Link href='/customers'>
            <a  className={pathname === '/customers' ? 'nav-link active' : 'nav-link'}>
              <span className="glyphicon glyphicon-ok-sign menuicon"></span><br/>
              <span className="menutext">Customers</span>
            </a>
          </Link> 
          </li>
      </ul>
      {authlinks}
</div>
    <style jsx global>{`
      :global(body) {
        margin: 0;
        font-family: -apple-system,BlinkMacSystemFont,Avenir Next,Avenir,Helvetica,sans-serif;
        background-color: #3d2115 !important;
      }
      #main-navbar{
        padding: 0px!important;
        margin: 0px!important;
      }
      .navbar {
        margin: 0px!important;
        padding: 0px!important;
        background-color: #3d2115 !important; // ora-brown #3d2115
        z-index: 1;
      }
      

      .navbar .navbar-nav> li > a{
        color: #BFA25E; 
        background-color: #3d2115 ;
        text-decoration: none;
        padding: 10px 0px!important;
      }
      
      #mynavbar > li > a:active, 
      #mynavbar > li > a.active,
      #auth-bar > li > a:active, 
      #auth-bar > li > a.active {
        color: #cc9933 !important;  // F7E29D
        background-color: #581414!important; // ora-yellow cc9933
        font-weight: bold!important;
      }
      
      #mynavbar > li > a:hover, 
      #mynavbar > li > a:focus,
      #auth-bar > li > a:hover, 
      #auth-bar > li > a:focus
      {
        color: #efa40d!important;
        background-color: #6c4433!important;
      }

     
#mynavbar{
  width: 375px!important;
  margin: 0px 0px !important;
  padding: 0px!important;
  display: inline-block;
}
#mynavbar > .nav-item,
#auth-bar > .nav-item,
#admin-bar > .nav-item{
  width: 50px!important;
  padding: 0px!important;
  margin: 0px 4px!important;
  text-align: center;
}

#mynavbar > .nav-item.contact{
  width: 75px!important;
  padding: 0px!important;
  text-align: center;
}
#mynavbar > li.nav-item,
#auth-bar > li.nav-item,
#admin-bar > li.nav-item
 {
  display: inline-block!important;
  text-align: center!important;
  padding: 0px!important;
}
#mynavbar > li > a,
#auth-bar > li > a,
#admin-bar > li > a,
{
  text-align: center!important;
  margin: 0px!important;
  padding: 0px!important;
}

#auth-bar > li > a > span.menuicon,
#mynavbar > li > a > span.menuicon{
  font-weight: 700;
  font-size: 18px;
  color: inherit!important;
  text-align: center;
}
#mynavbar > li > a > span.menuicon2{
  font-weight: 700!important;
  font-size: 18px!important;
  color: inherit!important;
  text-align: center;
}
#mynavbar > li > a > span.menutext,
#auth-bar > li > a > span.menutext,
#admin-bar > li > a > span.menutext{
  font-weight: 500!important;
  font-size: 12px!important;
  color: inherit!important;
  text-align: center;
}

#auth-bar {
    width:    59px!important;        
    display:  inline-block; 
    min-width:    59px!important;
    float: right;
    text-align: center;
    margin: 0px 0px!important;
        }
#admin-bar{
  display: block; 
  width: 300px!important;
   
   text-align: center;
   margin: 15px 0px;
}


#admin-bar > li > .admin{
  color: yellow!important;
}

#orders > .logout{
  color: yellow!important;
}
#logout > a :hover, #orders > a :hover{
  color: red!important;
}

#logout > span.menutext,
#auth-bar > li > a ,
#admin-bar > li > a ,
#logout, #orders {
  height: 100%;
  padding: 10px 0px;
  width: 55px;
  margin: 0px;
  color: yellow;
  text-align: center;
}

.mynav{
  padding: 0px 5px!important;
  margin: 0px!important;
}

@media (max-width:421px){
  .mynav{
    padding: 0px 0px!important;
  }
  #mynavbar{
    width: 80%!important;
    min-width: 280px!important;
  }
  #mynavbar > .nav-item,
  #auth-bar > .nav-item,
  #admin-bar > .nav-item{
    margin: 0px 0px!important;
  }
  #auth-bar {
    width: 55px!important;
    min-width: 50px!important;
  }

}
    `}</style>
 
 </nav>
)
  }
}    
  
const mapStateToProps = state => {
  return {
      loading: state.auth.loading,
      error: state.auth.error,
      isAuthenticated: state.auth.token !== null,
      authRedirectPath: state.auth.authRedirectPath,
      isAdmin: state.auth.isAdmin,
      authShow: state.auth.authShow,
      cartShow: state.floatCart.cartShow
  };
};

const mapDispatchToProps = dispatch => {
  return {
      onAuthClose: () => dispatch (actions.authClose()),
      onAuthOpen: () => dispatch (actions.authOpen()),
      onLogOut: () => dispatch (actions.authLogout()),
      onOrdersOpen: () => dispatch (actions.ordersOpen()), 
      onAddOpen: () => dispatch( actions.addOpen())
  };
};

export default connect( mapStateToProps, mapDispatchToProps)( Nav );
// export default Nav;
