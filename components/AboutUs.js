import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import Link from 'next/link';

export default () => (
    <div id="about-page" className="container-fluid">
        <div className="header">
            <h1 >orataiphathai.com</h1>
            <h2>Fashionable Thai Sarong Fabric - 
            Can wear every day!</h2>
        </div>
        <img src="../static/thai_sarong900.jpg" className="img-responsive" width="100%" height="200px" alt="Fashionable Thai Sarong Fabric" />
        <div className="body col-sm-7" >
            <div>
                <h2>About Us</h2>

                <h2>Wholesale and Retail of Fashaionable Sarong.</h2>
                <h2>Orataiphathai! Can Wear Every Day!</h2>
                <br/>
                <p>We love fashion, and we love Thai Sarong, so ...
                we bring you the best Thai Fashionable Sarong fabric.</p>
                
                <p>We have just a few sample here on our website and many more in our shop at BangNa, Thailand.</p>
                <p>We are constantly adding samples to our new website.</p>
                <p>It is always better to contact us directly for more info about our latest stock and patterns.</p>
                <p>We believe in personal service, so don't hesitate to contact us.<br/>
                We can send you more photo samples by LINE.
                Once you prove your order we can ship it in Thailand with 1 Day service.
                You can even choose COD, to pay as you get the products. (There is a small charge of 3% for COD).</p>
                
                <p>For any technical issue with the site please contact the website admin: aylon.spigel@gmail.com</p>
            </div>
            <div className="contact-us">
                <div className="contact-headline">
                    <h2 >Contact Us</h2>
                </div>
                    
                <div className="contact-left">
                    <div className="media-pair">
                        <img src="../static/phone.png" width="40px" className="img-icon" alt="orataiphathai Thai Sarong phone" />
                        <p className="info-text">084-1200065</p>
                    </div>
                    <div className="media-pair">
                        <img src="../static/mail.png" width="40px"  className="img-icon" alt="orataiphathai Thai Sarong Email" />
                        <a href="mailto:orataiphathai@gmail.com" className="info-text">
                    orataiphathai@gmail.com</a>
                    </div>
                     <div className="media-pair">
                        <img src="../static/facebook_icon.png" width="40px"  className="img-icon" alt="orataiphathai Email Thai Sarong" />
                        <a href="https://www.facebook.com/%E0%B8%AD%E0%B8%A3%E0%B8%97%E0%B8%B1%E0%B8%A2%E0%B8%9C%E0%B9%89%E0%B8%B2%E0%B9%84%E0%B8%97%E0%B8%A2-937874942959104/" 
                        target="_new" className="info-text">
                        Orataiphathai - Facebook Page</a>
                    </div>
                    <div className="media-pair">
                        <img src="../static/line.gif" width="40px" className="img-icon" alt="orataiphathai on LINE Thai Sarong" />
                        <a href="line://ti/p/@bcj6324n" className="info-text" target="_new" >Add Orataiphathai on LINE</a>
                    </div>
                </div>
                <div className="contact-right">
                    <div className="media-pair">
                        <img src="../static/line.gif" width="40px" className="img-responsive img-icon" alt="Thai Sarong Orataiphathai on LINE QR code" />
                        <p className="info-text">Add Oratai Phathai on LINE with this BARCODE</p>
                    </div>
                    <div className="code-img">
                        <img src="../static/LINE_QR_CODE1.jpg" width="200px" className="img-responsive" alt="Thai Sarong Orataiphathai on LINE QR code" />
                    </div>
                </div>
            </div>    
        </div>
        

        <div className="col-sm-5 ">
            
            <h2>Visit Us</h2>
            <p><img src="../static/store_front.jpg" className="img-responsive" width="500px" height="350px" alt="Fashionable Thai Sarong Fabric" />
            </p>
            <p>30/147 ซอย บางแก้ว 2 Tambon Bang Kaeo, 
                <br/>Amphoe Bang Phli, Chang Wat Samut Prakan 10540, Thailand
            </p>
            {/* gmap  */}
            <div className="embed-responsive embed-responsive-16by9">
            <iframe className="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15508.609535971156!2d100.6735125!3d13.6484929!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x42b66725ddc7bcc7!2z4Lij4LmJ4Liy4LiZ4Lit4Lij4LiX4Lix4Lii4Lic4LmJ4Liy4LmE4LiX4Lii!5e0!3m2!1sen!2sth!4v1538150428457" 
            width="100%" height="450" frameBorder="0" style={{ border:0 }} allowFullScreen></iframe>
            </div>
            <br/>
            <p>
                <b>Open Hours:</b> daily 9:00 AM - 06:00 PM
            </p>
        </div>
        

        <style jsx>{`
            div.contact-right > div.code-img > img{
                margin-left: 60px;
            }
            .contact-us{
                display: flex;
                flex-wrap: wrap;
                justify-content: space-between;
            }
            .contact-headline{
                flex: 0 1 100%;
            }
            .contact-left{
                flex: 0 1 50%;
            }
            .contact-right{
                flex: 0 1 50%;
            }
            #about-page{
                display: block;
                color: white;
                padding: 15px;
                
            }
            
            @media (min-width: 541px){
                #about-page > div > h1, h1{
                font-size: 3.5em!important;
                font-weight: 500!important;
                }
                #about-page > div > div > h2,
                #about-page > div > h2, h2 {
                    font-size: 28px!important;
                    font-weight: 100!important;
                }
            }
            @media (max-width: 540px){
                #about-page > div > div > h1, h1{
                font-size: 2.5em!important;
                font-weight: 500!important;
                }
                #about-page > div > div > h2,
                #about-page > div > h2, h2 {
                    font-size: 32px!important;
                    font-weight: 100!important;
                }
                a.info-text{
                    order: 2;
                    width: 200px!important;
                }
                p.info-text{
                order: 2;
                width: 200px!important;
                }

            }

            

            #about-page > div.body, 
            #about-page > div > p {
                font-size: 1.4em!important;
                font-weight: 300!important;
            }

            #about-page > .header{
                 text-align: center
            }

            .media-pair{
                display: flex;
                justify-content: flex-start;
                align-items: center;
            }
            .img-icon{
                order: 1;
                width: 50px;
                margin: 5px 10px 5px 0;
                vertical-align: middle;
            }
            a.info-text,p.info-text{
                order: 2;
                width: 100%;
                margin: 0px;
                font-size: 1em;
                vertical-align: middle;
                margin: auto 0;
                background-color: #000000!important;
                color: white;
            }

        `}</style>
    </div>

)

