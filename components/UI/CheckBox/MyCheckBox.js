import React, { Component } from 'react';
import PropTypes from "prop-types";

class MyCheckbox extends Component {
  state = {
    isChecked: false,
  }

  toggleCheckboxChange = () => {
    this.setState(({ isChecked }) => (
      {isChecked: !isChecked }
    ));
    this.props.handleCheckboxChange(this.state.isChecked,this.props.value);
  }

  render() {
    const { label } = this.props;
    const { isChecked } = this.state;

    return (
      <div className="mycheckbox">
          <input id={label}
            type="checkbox"
            className="checkbox"
            value={label}
            checked={isChecked}
            onChange={this.toggleCheckboxChange}
          />
            <label htmlFor={label}></label> 
        <span className="productColor">{label}</span>
        {/* {(this.state.isChecked)?' labelon':'productColor labeloff'} */}

<style jsx >{`

.checkbox {
    display: none;
  }
  
  .checkbox + label {
    display: inline-block;
    position: relative;
    width: 30px;
    height: 30px;
    background: white;
    border-radius: 6px;
    padding: 0px;
    margin-right: 5px;
    cursor: pointer;
    transition: all 500ms ease;
    box-shadow: 0 0px 10px rgba(0, 0, 0, 0.2) inset;
    border: 1px solid black;
  }
  .checkbox + label:before {
    content: "0";
    width: 30px;
    height: 30px;
    position: absolute;
    left: 0px;
    top: 0px;
    text-align: center;
    line-height: 26px;
    font-family: "Cabin", sans-serif;
    font-size: 20px;
    font-weight: bold;
    color: black;
    letter-spacing: 3px;
    text-transform: uppercase;
    transition: left 500ms ease, color 500ms ease, -webkit-transform 150ms ease;
    transition: left 500ms ease, color 500ms ease, transform 150ms ease;
    transition: left 500ms ease, color 500ms ease, transform 150ms ease, -webkit-transform 150ms ease;
  }
  .checkbox + label:active:before {
    -webkit-transform: scale(0.95);
            transform: scale(0.95);
  }
  
  .checkbox:checked + label {
    background-color: yellow;
  }
  .checkbox:checked + label:before {
    content: "1";
    color: black;
  }
  .productColor{
      font-size: 18px;
      display: inline-block;
      vertical-align: top;
  }
  .mycheckbox{
    display: inline-block;
    white-space: nowrap;
    
  }
`}</style>
</div>
    );
  }
}


MyCheckbox.propTypes = {
  label: PropTypes.string.isRequired,
  handleCheckboxChange: PropTypes.func.isRequired,
};

export default MyCheckbox;