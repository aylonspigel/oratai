import React, { Component } from 'react';
import PropTypes from "prop-types";

class Checkbox extends Component {
  state = {
    isChecked: false,
  }

  toggleCheckboxChange = () => {
    this.setState(({ isChecked }) => (
      {isChecked: !isChecked }
    ));
   // console.log('this.props.value',this.props.value );
  //  console.log('this.state.isChecked',this.state.isChecked );
    this.props.handleCheckboxChange(this.state.isChecked,this.props.value);
  }

  render() {
    const { label } = this.props;
    const { isChecked } = this.state;

    return (
      <div className="checkbox">
        <label className="label">
          <input
            type="checkbox"
            value={label}
            checked={isChecked}
            onChange={this.toggleCheckboxChange}
          />

          <span className={(this.state.isChecked)?'labelon':'labeloff'}>{label}</span>
        </label>
        <style jsx global>{`
            span.labeloff{
              color: black!important;
            }
            span.labelon{
                color: red!important;
                font-weight: bold;
            }
             .checkbox{
              margin-top: 0px!important;
              margin-bottom: 0px!important;
          }
        `}</style>
      </div>
    );
  }
}


Checkbox.propTypes = {
  label: PropTypes.string.isRequired,
  handleCheckboxChange: PropTypes.func.isRequired,
};

export default Checkbox;