import React from 'react';
import Link from 'next/link';

export default ({ pathname }) => (

<div className='container-fluid customers'>
    <div className="customers-list">
        <div className="customer">
            <div className="customer-img"><img src="../static/customer_0001.jpg" title="Thai Sarong fashion fusion" alt="Thai Sarong fashion fusion" /></div>
            <div className="customer-text"><span>This fashion designer has made a great fusion of Thai Sarong Fabric with his modern wear set.</span></div>
        </div>
        <div className="customer">
            <div className="customer-img"><img src="../static/customer_0002.jpg" title="Thai Sarong Bed Covers" alt="Thai Sarong Bed Covers" /></div>
            <div className="customer-text"><span>This spa with its traditional Thai massage used Thai sarong fabric to add a clean and traditional look to its massage suites.</span></div>
        </div>
        <div className="customer">
            <div className="customer-img"><img src="../static/customer_0003.jpg" title="Sarong kitchen wear" alt="Thai Sarong kitchen wear" /></div>
            <div className="customer-text"><span>In Europe, they love Thai sarong fabric for kitchen wear.</span></div>
        </div>
        <div className="customer">
            <div className="customer-img"><img src="../static/customer_0004.jpg" title="Sarong elephant figurine" alt="Thai Sarong elephant figurine" /></div>
            <div className="customer-text"><span>This customer has taken the Sarong  Fabric to another level with its Sarong elephant figurine.</span>
            <br/><span className="yellow">credit by: </span>
            <span>A-Zig.</span>
            </div>
        </div>
        <div className="customer">
            <div className="customer-img"><img src="../static/customer_00051.jpg" title="Kids Fashion Sarong" alt="Kids Fashion Thai Sarong" /></div>
            <div className="customer-text"><span>Kids Fashion Sarong.</span>
            <br/><span className="yellow">credit by: </span>
            <span>Yada Baby Shop.</span>
            </div>
        </div>
        <div className="customer">
            <div className="customer-img"><img src="../static/aimtam.jpg" title="AimTam thaibaby clothes" alt="AimTam thaibaby clothes Thai Sarong" /></div>
            <div className="customer-text"><span>Baby Clothes.</span>
            <br/><span className="yellow">credit by: </span>
            <span>AimTam thaibaby clothes.</span>
            </div>
        </div>
    </div>

<style jsx>{`
    .customers {
          /* display: block;
          width: 100%;
          margin: 0px;
          padding: 20px; */
          background-color: #000000;
          color: white;
    }
    .center-text{
        margin: 0 auto;
        text-align: center;
    }
    .customers-list{
        display: flex;
        width: 100%;
        flex-wrap: wrap;
        justify-content: space-between;
        padding: 10px;
    }
    .customer{
        display: flex;
        width: 45%;
        justify-content: space-evenly;
        margin: auto;
        padding: 10px;
        margin-bottom: 20px;
        border-bottom: 1px dashed white;
    }
    .customer-img{
        flex: 0 0 45%;
        padding:5px;
    }
    .customer-img > img{
        width:100%;
        height: auto;
    }
    .customer-text{
        flex: 1 0 50%;
        padding: 5px;
        white-space: normal; 

    }
    .customer-text > span{
        font-size: 1.4em;
        white-space: normal;
        padding: 0px 5px;
    }

    .yellow{
        color: yellow;
    }


    @media (max-width: 650px){
        .customer{
            width: 100%;
            flex-wrap: wrap;
        }
        .customer-text > span{
            font-size: 1em;
        }
        .customer-img{
            flex: 0 0 100%;
        }
        .customer-text{
            flex: 0 0 100%;
        }
    }


`}</style>
</div>
)