import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../store/actions/index';
import axios from '../axios-firebase';
import * as productGroupData from '../shared/productGroup.json';
import Modal from './UI/Modal/Modal';
//import EditProdForm from './editProdForm';

class ProductDisplay extends Component {

    constructor(props) {
        super(props);
    }

   
    state = {
        formIsValid: true,
        current_product_index: this.props.pin,
        currentArrSize: this.props.productsInfo.length
    };

    componentWillReceiveProps(nextProps, nextState) {
        if(this.props.productsInfo && nextProps.productsInfo.length !== this.props.productsInfo.length){
            // make a new filteredProduct
            this.setState({currentArrSize:nextProps.productsInfo.length})
        }
        // if(this.state.current_product_index !== nextState.current_product_index){
        //     let product = this.props.productsInfo[this.state.current_product_index];
        //     this.props.onUpdateCurrentProductId(product.id)
        // }
    }

    closeProductModal = () => {
       // this.props.onCloseProduct();
        this.props.closeShow();

    }

    //let currentArrSize = this.props.productsInfo.length;
    prevProd= () => {
        if(this.state.current_product_index>=1){
            this.setState({current_product_index: this.state.current_product_index-1});
        }
        
    }
    nextProd= () => {
        console.log('currentArrSize',this.state.currentArrSize);
        if(this.state.current_product_index<this.state.currentArrSize-1){
            this.setState({current_product_index: this.state.current_product_index+1});
        }
    }
    handleDelProd = (pid) => {
        //console.log('id: ',event.target.id);
        this.props.onDelProduct(pid);
        this.props.closeShow();
        this.props.onFetchAllProducts();
    }

    handleEditProd = (pid) => {
        this.props.onOpenEdit(pid);
        this.props.onUpdateCurrentProductId(pid)
        console.log("this.props.editShow",this.props.editShow);
        console.log("pid",pid);
        //this.props.productsInfo[this.props.pin]);
    }

    


    render () {

        let product = this.props.productsInfo[this.state.current_product_index];
        
    
        return (
        <div id="productDetails">
        
            <div className="back-btn" onClick={() => this.closeProductModal()}>
                <span className="glyphicon glyphicon-arrow-left"></span> 
                <span > Back To Products Menu</span>
            </div>
            <div className="group-title">
                Group: {productGroupData.productGroup[product.group_id-1].title}
                <p className="product-title">Product Name: {product.title}</p>
            </div>
            <div className="product-pager">
                <button className={(this.state.current_product_index>=1)?'active':'disabled'} 
                    onClick={() => this.prevProd()}> <span className="glyphicon glyphicon-arrow-left"></span> Previous Product</button>
                
                <button className={(this.state.current_product_index<this.state.currentArrSize-1)?'active':'disabled'} 
                    onClick={() => this.nextProd()}> Next Product  <span className="glyphicon glyphicon-arrow-right"></span></button>
            </div>

            {(this.props.isAdmin===true)?<div className="admin-btn">
                <button onClick={() => this.handleDelProd(product.id)}>Delete This Product</button>
                <button onClick={() => this.handleEditProd((product.id.toString()))}>Edit This Product {product.id}</button>
            </div> :null}
            <div className="product-details">
                <div className="product-img-container">
                <img src={"../static/" + product.photo_url} 
                        onError={(e)=>{e.target.src="../static/colors1.jpg"}}
                        className="media-object prd-img"
                        alt="Thai Sarong Orataiphathai" />
                </div>
                <div className="product-desc-container"><p>{product.description}</p></div>
            </div>

            <style jsx>{`
                #productDetails {
                    color: white;
                    background-color: black;
                }
                .admin-btn button{
                    color: black;
                    margin: 10px;
                    padding: 3px;
                    border-radius: 10px;
                    background-color: #cccccc;
                }
                .admin-btn button:hover{
                    background-color: #999999;
                    cursor: pointer;

                }
                .back-btn{
                    border-radius: 20px;
                    border: 2px solid black;
                    padding: 10px;
                    margin: 10px;
                    cursor: pointer;
                    color: white;
                    background-color: #cccccc;
                    font-size: 18px;
                    box-shadow: 4px 4px 5px grey;
                    font-weight: 300;
                }
                .back-btn:hover, .back-btn:focus{
                    color: white;
                    background-color: #999999;
                    border: 2px solid black;
                    font-weight: 700;
                    box-shadow: 9px 9px 5px grey;
                    text-shadow: 1px 1px 2px black, 0 0 25px yellow, 0 0 5px gray;
                }

                .group-title {
                    text-align: center;
                    font-size: 24px;
                    width: 100%;
                    margin: 20px;
                    padding: 0px ;
                    box-sizing: border-box;
                }
                
                .product-title{
                    color: white;
                    text-align: center;
                    font-size: 18px;
                }
                .product-pager > button {
                    color: black;
                    font-size: 18px;
                    margin: 10px;
                    padding: 3px;
                    border-radius: 10px;
                    background-color: #cccccc;
                }
                .product-pager{
                    color: black;
                    text-align: center;
                    font-size: 18px;
                    display: flex;
                    justify-content: space-between;
                }

                .product-pager> button.disabled{
                    opacity: 0.3;
                    cursor: not-allowed;
                }
                .product-pager> button{
                    background-color: #cccccc;
                }
                .product-pager> button:hover, .product-pager> button:focus{
                    background-color: #999999;
                }

                .product-details{
                    display: flex;
                    width: 100%;
                    justify-content: space-evenly;
                    flex-wrap: nowrap;
                }
                .product-img-container{
                    padding: 15px;
                    flex: 1 0 40%;
                }
                .product-img-container > img{
                    width: 100%;
                }
                .product-desc-container{
                    padding: 15px;
                    flex: 1 0 60%;
                }

            
                @media (max-width: 700px) {
                    .product-details{
                        display: flex;
                        width: 100%;
                        justify-content: center;
                        flex-wrap: wrap;
                    }
                    .product-img-container{
                        padding: 15px;
                        flex: 1 0 100%;
                    }
                    .product-desc-container{
                        padding: 15px;
                        flex: 1 0 100%;
                    }
                }
            `}</style>
        </div>
        );
    }
}



const mapStateToProps = state => {
    return {
        formIsValid: state.formIsValid,
        token: state.auth.token,
        userId: state.auth.userId,
        isAdmin: state.auth.isAdmin,
        editShow: state.allProducts.editShow,
        currentProductId: state.allProducts.currentProductId,

    }
};
// product, this.props.token
const mapDispatchToProps = dispatch => {
    return {
        onGetisAdmin: (email) => dispatch(actions.getisAdmin(email)),
        onCloseProduct:     () => dispatch( actions.closeProduct()),
        onDelProduct: (pid) => dispatch( actions.delProduct(pid)),
        onFetchAllProducts: () => dispatch( actions.fetchAllProducts()),
        onOpenEdit: (pid) => dispatch( actions.openEdit(pid)),
        onCloseEdit: () => dispatch( actions.closeEdit()),
        onUpdateCurrentProductId: (id) => dispatch( actions.updateCurrentProductId(id)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDisplay, axios);