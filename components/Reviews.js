import React from 'react';
import Link from 'next/link';

export default ({ pathname }) => (

<div className='container-fluid reviews'>
    <div className="reviews-list">
        <div className="review">
            <div className="review-img"><img src="../static/rapin.jpg" title="" alt="" /></div>
            <div className="review-text">
                <span>Name: Rapin Sanyapan</span><br/>
                <span>Review: ส่งไวค่ะ สีสวยถูกใจ</span><br/>
                <span>Translation: Product was sent fast with beautiful colors. </span><br/>
                <span className="glyphicon glyphicon-thumbs-up"></span><span className="glyphicon glyphicon-thumbs-up"></span><span className="glyphicon glyphicon-thumbs-up"></span>
            </div>
        </div>
        <div className="review">
            <div className="review-img"><img src="../static/ong.jpg" title="" alt="" /></div>
            <div className="review-text">
                <span>Name: อนงค์ บุญเอี่ยม</span><br/>
                <span>Review: ผ้าสวยมากคะ ถูกใจมากแม่ก็ชมว่าผ้าสวยขอแบ่งไปตั้ง6ผืน</span><br/>
                <span>Translation: Very nice fabric. Very affordable. I also noticed that the fabric is very beautiful. I ordered 6 blocks of 40m each. </span><br/>
                <span className="glyphicon glyphicon-thumbs-up"></span><span className="glyphicon glyphicon-thumbs-up"></span><span className="glyphicon glyphicon-thumbs-up"></span>
            </div>
        </div>
        <div className="review">
            <div className="review-img"><img src="../static/sujit.jpg" title="" alt="" /></div>
            <div className="review-text">
                <span>Name: Saisamorn Sujit</span><br/>
                <span>Review: ส่งไวส่งจริงผ้าดีมากสั่งมาหลายรอบแล้วค่ะ</span><br/>
                <span>Translation: Fast delivery. Very good fabric. Ordered many times. </span><br/>
                <span className="glyphicon glyphicon-thumbs-up"></span><span className="glyphicon glyphicon-thumbs-up"></span><span className="glyphicon glyphicon-thumbs-up"></span>
            </div>
        </div>
        <div className="review">
            <div className="review-img"><img src="../static/surin.jpg" title="" alt="" /></div>
            <div className="review-text">
                <span>Name: Laongdow Surin</span><br/>
                <span>Review: ของถึงแล้วน่ะค่ะบริการดีมากค่ะทีนี้ครั้งแรกทีใช้บริการประทับใจมากเร็วมากเพิ่งสั่งของเมื่อวานวันี้้เช้าของถึงแล้ว</span><br/>
                <span>Translation: Items have just arrived. Very good service. It's the first time I use this service. Very impressed. I ordered it only yesterday and it arrived this morning. </span><br/>
                <span className="glyphicon glyphicon-thumbs-up"></span><span className="glyphicon glyphicon-thumbs-up"></span><span className="glyphicon glyphicon-thumbs-up"></span>
            </div>
        </div>
    </div>

<style jsx>{`
    .reviews {
          /* display: block;
          width: 100%;
          margin: 0px;
          padding: 20px; */
          background-color: #000000;
          color: white;
    }
    .center-text{
        margin: 0 auto;
        text-align: center;
    }
    .reviews-list{
        display: flex;
        width: 100%;
        flex-wrap: wrap;
        justify-content: space-between;
        padding: 10px;
    }
    .review{
        display: flex;
        width: 45%;
        height: auto;
        justify-content: space-evenly;
        flex-wrap: wrap;
        margin: 10px auto ;
        padding: 10px;
        margin-bottom: 20px;
        border-bottom: 1px dashed white;
        align-items: flex-start;
    }
    .review-img{
        flex: 0 0 100%;
        padding:5px;
    }
    .review-img > img{
        width:100%;
        height: auto;
    }
    .review-text{
        flex: 0 0 100%;
        padding: 5px;
        white-space: normal; 

    }
    .review-text > span{
        font-size: 1.4em;
        white-space: normal;
        padding: 0px 5px;
    }

    @media (max-width: 600px){
        .review{
            width: 100%;
            flex-wrap: wrap;
        }
        .review-text > span{
            font-size: 1em;
        }
`}</style>
</div>
)
