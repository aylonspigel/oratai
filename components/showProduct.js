import React, { Component } from 'react';
import { connect } from 'react-redux';
import Router from 'next/router';
import Button from './UI/Button/Button';
import Spinner from './UI/spinner/spinner';
import axios from '../axios-firebase';
import Input from './UI/Input/Input';

import * as actions from '../store/actions/index';
import { updateObject, checkValidity } from '../shared/utility';
import * as colors from '../store/colors';
import Checkbox from './UI/CheckBox/CheckBox';

class ShowProductForm extends Component {

    constructor(props) {
        super(props);
        this.basketChange = this.basketChange.bind(this);
    }

    state = {
        formIsValid: true,
        productData: {},
        productStock: {},
        productInfo: {},
        p_in: this.props.pin,
        cartOrder: {},
        orderArray: null,
        patternArray: []
    };
    
   

    basketChange = ( checked,colorid ) => {
        //console.log('colorid',colorid);
        let color = colors.COLORS[colorid];
        //console.log('color',color);
        //console.log('checked',checked);
        let productObj = this.props.myproductInfo;
        //console.log(' productObj',productObj);
        //console.log(' before',this.state.orderArray);
        let product_id = `${productObj.productData.pattern_id}${colorid}`
        let orderArray = (this.state.orderArray===null)?[]:[...this.state.orderArray];
        //checked  = handleCheckboxChange().checked;
        //let patternArray = (this.state.patternArray===null)?[]:[...this.state.patternArray];
        let patternArray = this.state.patternArray;

        if (checked){
            //remove product
            if(JSON.stringify(orderArray).includes(product_id)){
                orderArray=orderArray.filter(arr => arr.product_id !== product_id);
            }
            if( !JSON.stringify(orderArray).includes(product_id) &&
                !JSON.stringify(orderArray).includes(productObj.productData.pattern_id)){  
                    patternArray=patternArray.filter(ptr => ptr !== productObj.productData.pattern_id)
            }
        }
        else{
            // add product
            //console.log('notchecked',checked);
            if(!JSON.stringify(orderArray).includes(product_id)){
                let productToAdd = {
                    product_id :  product_id,  
                    pattern_id : productObj.productData.pattern_id,
                    colorId: colorid,
                    color: color,
                    retail_price: productObj.productData.retail_price,
                    wholesale_price: productObj.productData.wholesale_price,
                    description: productObj.productData.description,
                    photo_url: productObj.productData.photo_url,
                    qty: 1
                }
                orderArray.push(productToAdd);

                if( patternArray===[]){
                    patternArray.push(productToAdd.pattern_id);
                }else if( patternArray.includes(productToAdd.pattern_id)){
                    //console.log('pattern is already in array');
                }else{
                    patternArray.push(productToAdd.pattern_id);
                }
            }

        }
        this.setState({
            orderArray: orderArray,
            patternArray: patternArray
        })
        //console.log('after',this.state.orderArray);
        //console.log('patternArray',this.state.patternArray);
        //return orderArray;
    }

    orderHandler = ( event ) => {
        event.preventDefault();
        //console.log('event.data',event.data)
    }

    Basket2Cart = ( event ) => {
        event.preventDefault();
        //console.log('event',event)
        this.props.onUpdateCart(this.state.orderArray,this.state.patternArray);
        this.props.onShowClose();
        
    }

    render () {

        let delButton = (p_id) => {
            if(this.props.isAdmin===true){
            return <button className='btn btn-danger' id={p_id} 
                onClick={() => this.props.handleDelete()}
                >Delete</button>;
            } else { return null}
        }
        let editButton = (p_in) => {
            if(this.props.isAdmin===true){
            return <button className='btn btn-success' 
                p_in={p_in}
                onClick={() => this.props.handleEdit(p_in)} 
                >Edit {p_in}</button>;
            } else { return null}
        }
        // //console.log('Edit render props.myproductInfo' + this.props.myproductInfo);
        // //console.log('Edit render state.productForm' + JSON.stringify(this.state.productForm));
        
        let productObj = this.props.myproductInfo;

        const colorsList = [...colors.COLORS];
        let buyingForm = "";
        if(productObj){
        buyingForm = (
            <div className="buybox">
    <form className="buyForm" style={{color: 'black'}} onSubmit={this.Basket2Cart}>
        <div><h3>Please choose the colors:</h3></div>
        <input type="hidden" name="patternID" value="{{productObj.productData.pattern_id}}" />
        <div className="colors">
            {colorsList.map( (color,index) => (
                <div    key={index} 
                        className="formLine" 
                >
                    <Checkbox
                        label={color}
                        handleCheckboxChange={this.basketChange} 
                        key={index}
                        value={index}
                    /> 
                </div>
            )) }
        </div>
    </form>
                <Button type='submit' btnType='Success'>Update Your Basket</Button>
                <style jsx global>{`
                .buybox{
                    width: 70vw;
                }
                .Success {
                    padding: 10px 20px 10px 20px!important;

                }
                button.Success:hover {
                    background: rgba(0,0,0,0);
                    color: #3a7999;
                    box-shadow: inset 0 0 0 3px #3a7999;
                }
                .colors > div > div > label > span{
                    color: black!important;
                }
                // .colors > div:nth-child(3) > div > label > span.labeloff{
                //     color: black!important;
                // }
              
                .colors{
                    display: flex;
                    flex-wrap: wrap;
                    align-items: flex-start;
                    justify-content: flex-start;
                }

                .formLine{
                    flex: 0 1 18%;
                    text-align: left;
                    padding: 0 5px;
                    height: 30px;
                }
                .buyForm > div > h3{
                    font-size: 1em!important;
                }
                
                `}</style>
           </div>
        )
    }   

        let productData = <Spinner />;
        // //console.log('this.props.isAdmin',this.props.isAdmin);
        if ( this.props.loading ) {
            productData = <Spinner />;
        } else {
           // let productObj = this.props.myproductInfo;
            if(productObj){
            productData = (
                <div key={this.props.pin}  className="bigCard">
                 <div className="product-img">
                        <img src={"../static/" + productObj.productData.photo_url} 
                                onError={(e)=>{e.target.src="../static/sarong1.png"}}
                                className="media-object productImg img-responsive"
                                alt="Thai Sarong Orataiphathai" 
                        />
                        {(this.props.isAdmin===true)?<span>photo name1: {productObj.productData.photo_url}</span>:""}
                </div>
                <div className="product-info">
                  <h1 className="product-heading">{productObj.productData.product_name}</h1>
                  <span>Pattern no.: {productObj.productData.pattern_id}</span><br/>
                  <span>Description: {productObj.productData.description}</span><br/>
                  <span>Price: (1-12 pieace): {productObj.productData.retail_price}</span><br/>
                  <span>Price: (12+ pieace): {productObj.productData.wholesale_price}</span><br/>
                  {delButton(productObj.product_id)}
                  {editButton(this.props.pin)}
                </div>
                <style jsx >{`
            .bigCard{
                display: flex;
                flex-wrap: wrap;
                justify-content: space-between;
                width: 70vw;
            }
            .product-img{
                flex: 0 1 50%;
                margin: 0px auto auto auto;

            }
            .productImg{
                max-width: 400px;
                max-height: 400px;
                width: 30vw;
                height: auto;
            }
            .product-info{
                flex: 0 1 50%;
            }
            @media (max-width: 420px) {
                .product-img{
                    flex: 0 1 75%;
                    margin: 0px auto auto auto;
    
                }
                .product-info{
                    flex: 0 1 75%;
                }
                .productImg{
                    max-width: 350px;
                    max-height: 350px;
                    width: 80%;
                    height: auto;
                }
            }
            `}</style>
              </div>
            );}
            else {
                productData = <p>No Data</p>
            }
        }

        return (
            <div className='ProductData'>
                    {/* <p>this.state.productData: {JSON.stringify(this.state.productData)}</p>
                    <p>this.props.myproductInfo: {JSON.stringify(this.props.myproductInfo)}</p> */}
                {productData}
                {buyingForm}
            <style jsx >{`
            .pro-form {
                color: black;
            }

            .ProductData {
                color: black!important;
                margin: 0px auto 0px auto;
                width: 70vw;
                text-align: center;
                padding: 0px ;
                box-sizing: border-box;
                height: 630px;
            }
            
            @media (min-width: 600px) {
                .ProductData {
                    width: 70vw;
                }
            }
            `}
            </style>
            </div>
        );
    }
}

 
const mapStateToProps = state => {
    return {
        product: state.productData,
        productId: state.productId,
        formIsValid: state.formIsValid,
        token: state.auth.token,
        userId: state.auth.userId,
        isAdmin: state.auth.isAdmin
    }
};
// product, this.props.token
const mapDispatchToProps = dispatch => {
    return {
        onGetisAdmin: (email) => dispatch(actions.getisAdmin(email)),
        onUpdateCart: (cartProducts, patternArray) => dispatch(actions.updateCart( cartProducts,patternArray)),
        onShowClose: (email) => dispatch(actions.showClose()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowProductForm, axios);