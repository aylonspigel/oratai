import React from 'react';
import Link from 'next/link';
import Auth from '../containers/Auth/Auth';


export default ({ pathname }) => (

<div className='container-fluid home'>
    <div className="col-sm-12 ontop">
        <div id="welcome" className='col-12 align-center' >
            <h1>Welcome to our NEW <b>Thai Sarong</b> web site. </h1>
            <p>We are still adding new products and Thai Sarongs to our catalog every day so be sure to come back again. <br/>
            You can see some of our latest designs and promotions on our Facebook page.<br/> 
            Click the link below to get there now.</p>
            <p><a className="flink" href="https://www.facebook.com/pages/category/Clothing-Store/%E0%B8%AD%E0%B8%A3%E0%B8%97%E0%B8%B1%E0%B8%A2%E0%B8%9C%E0%B9%89%E0%B8%B2%E0%B9%84%E0%B8%97%E0%B8%A2-937874942959104/?locale2=th_TH" target="_new">
                Orataiphathai Thai Sarong Facebook Page!</a></p>
        </div>
        <div id="myCarousel" className="carousel slide" data-ride="carousel">
                <ol className="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" className="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                    <li data-target="#myCarousel" data-slide-to="4"></li>
                    <li data-target="#myCarousel" data-slide-to="5"></li>
                    <li data-target="#myCarousel" data-slide-to="6"></li>
                </ol>

                <div className="carousel-inner">
                   
                    <div className="item active">
                        <img src="../static/slider/slider-0001.jpg" alt="Oratai Phathai Thai Sarong" />
                    </div>
                    <div className="item ">
                        <img src="../static/slider/slider-0002.jpg" alt="Oratai Phathai Thai Sarong" />
                    </div>
                    <div className="item ">
                        <img src="../static/slider/slider-0003.jpg" alt="Oratai Phathai Thai Sarong" />
                    </div>
                    <div className="item ">
                        <img src="../static/slider/slider-0004.jpg" alt="Oratai Phathai Thai Sarong" />
                    </div>
                    <div className="item ">
                        <img src="../static/slider/slider-0005.jpg" alt="Oratai Phathai Thai Sarong" />
                    </div>

                    <div className="item">
                        <img src="../static/slider/slider-0006.jpg" alt="Oratai Phathai Thai Sarong"/>
                    </div>

                    <div className="item">
                        <img src="../static/slider/slider-0007.jpg" alt="Oratai Phathai Thai Sarong" />
                    </div>
                </div>

    
                <a className="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span className="glyphicon glyphicon-chevron-left"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="right carousel-control" href="#myCarousel" data-slide="next">
                    <span className="glyphicon glyphicon-chevron-right"></span>
                    <span className="sr-only">Next</span>
                </a>
        </div>
    </div>
    <div className="col-sm-12 signup">
        <p>Please sign up here.</p>
    </div>
        <Auth />
    <style jsx>{`
    .ontop{
        z-index: 100;
    }
    #welcome > p > a.flink{
        color: lightblue;
        cursor: pointer;
    }
    #welcome > p > a.flink:hover, #welcome > p > a.link:focus{
            color: yellow;
        }

    .signup{
        text-align: center;
        font-size: 24px;
        margin: 40px 0px 0px 0px;
        padding: 0px;
    }
    
    .center-text{
        margin: 0 auto;
    }
    #myCarousel > div > div > img{
        margin: 0 auto;
    }
    #welcome > p{
        font-size: 18px;
    }
        .home {
          display: block;
          position: relative;
          margin: 0px;
          padding: 20px;
          background-color: #000000;
          color: white;
          height: 100%;
        }
        h1 {
          font-size: 34px;
        }
        
        h2 {
          font-size: 18px;
        }
        .align-center{
            text-align: center;
        }
    `}</style>

</div>
)