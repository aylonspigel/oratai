import NextHead from 'next/head';
import { string } from 'prop-types';

const defaultDescription = 'OrataiPhathai Website';
const defaultKeywords = "orataiphathai, Thai Sarong, Thai fabric, fashionable sarong, Sarong";
const defaultOGURL = 'http://www.orataiphathai.com';
const defaultOGImage = '';


const Head = (props) => (
  <NextHead>
    <meta charSet="UTF-8" />
    
    <title>{props.title || ''}</title>
    <meta name="description" content={props.description || defaultDescription} />
    <meta name="author" content="Aylon Spigel" />
    <meta name="keywords" content={(props.keywords)? props.keywords + ", " + defaultKeywords : defaultKeywords} />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /> 
    <link rel="icon" sizes="48x48" href="/static/logo.png" />
    <link rel="apple-touch-icon" href="/static/logo.png" />
    <link rel="mask-icon" href="%PUBLIC_URL%/logo.svg" />


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />


    <link rel="icon" href="/static/favicon.ico" />
    <meta property="og:url" content={props.url || defaultOGURL} />
    <meta property="og:title" content={props.title || ''} />
    <meta property="og:description" content={props.description || defaultDescription} />
    <meta name="twitter:site" content={props.url || defaultOGURL} />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:image" content={props.ogImage || defaultOGImage} />
    <meta property="og:image" content={props.ogImage || defaultOGImage} />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />

    {/* <!-- Facebook Pixel Code --> */}
<>
    <script dangerouslySetInnerHTML={
        { __html: `!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                  n.queue=[];t=b.createElement(e);t.async=!0;
                  t.src=v;s=b.getElementsByTagName(e)[0];
                  s.parentNode.insertBefore(t,s)}(window, document,'script',
                  'https://connect.facebook.net/en_US/fbevents.js');
                  fbq('init', '484559961986806');
                  fbq('track', 'PageView');` }
    }
    />
    <noscript dangerouslySetInnerHTML={{ __html: `<img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=484559961986806&ev=PageView&noscript=1" />` }}
    />
  </>
  </NextHead>
);

Head.propTypes = {
  title: string,
  description: string,
  url: string,
  ogImage: string
}

export default Head;
