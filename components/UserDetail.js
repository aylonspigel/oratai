import React, { Component } from 'react';
import { connect } from 'react-redux';

import Button from './UI/Button/Button';
import Spinner from './UI/spinner/spinner';
import Input from './UI/Input/Input';

import * as actions from '../store/actions/index';
import { updateObject, checkValidity } from '../shared/utility';

class UserDetail extends Component {
    initialState = {
        orderForm: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            street: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Street'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            zipCode: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'ZIP Code'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    maxLength: 5,
                    isNumeric: true
                },
                valid: false,
                touched: false
            },
            country: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Country'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Your E-Mail'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            phone: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Phone:'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 10,
                    maxLength: 12,
                    isNumeric: true
                },
                valid: false,
                touched: false
            },
        },
        formIsValid: false
    }

    state = {...this.initialState}

//     componentDidMount() {
//         // AJAX CALL
//         const userId = localStorage.getItem('userId');
//         this.props.onGetUserDetails(userId);
//         console.log('didMount this.props.userDetails', this.props.userDetails);
//    }
    
//    componentWillUpdate(){
//     console.log('will this.props.userDetails', this.props.userDetails);
//     if(this.props.userDetails && this.props.userDetails !== false){ 
//         console.log('will2 this.props.userDetails', this.props.userDetails);
//         this.state.orderForm.name.value = this.props.userDetails.name; 
//         this.state.orderForm.street.value = this.props.userDetails.street; 
//         this.state.orderForm.zipCode.value = this.props.userDetails.zipCode; 
//         this.state.orderForm.country.value = this.props.userDetails.country; 
//         this.state.orderForm.email.value = this.props.userDetails.email; 
//         this.state.orderForm.phone.value = this.props.userDetails.phone; 
//         }
//    }
//    componentWillReceiveProps(nextProps) {
//     if(this.props.userDetails !== null || this.props.userDetails !== false){ 
//         if(this.state.userDetails !== nextProps.userDetails){
//         this.state.orderForm.name.value = nextProps.userDetails.name; 
//         this.state.orderForm.street.value = nextProps.userDetails.street; 
//         this.state.orderForm.zipCode.value = nextProps.userDetails.zipCode; 
//         this.state.orderForm.country.value = nextProps.userDetails.country; 
//         this.state.orderForm.email.value = nextProps.userDetails.email; 
//         this.state.orderForm.phone.value = nextProps.userDetails.phone; 
//         }
//     }
//    }

    orderHandler = ( event ) => {
        event.preventDefault();
  
        const formData = {};
        for (let formElementIdentifier in this.state.orderForm) {
            formData[formElementIdentifier] = this.state.orderForm[formElementIdentifier].value;
        }
        let userDetails = formData;
       // localStorage.setItem("userDetails", JSON.stringify(userDetails));
       // this.props.updateUserDetail(userDetails);
       // this.props.cart2Order(userDetails);
       const userId = localStorage.getItem('userId');
       userDetails = updateObject(userDetails,{"userId":userId })
    //   console.log('user detail submited', userDetails);
       localStorage.setItem('userDetails', JSON.stringify(userDetails));
       //this.props.formValid=this.state.formIsValid;
       
       this.props.addUserDetails(userDetails);
       this.props.checkOut(this.state.formIsValid);
      // console.log('user .state.formIsValid', this.state.formIsValid);
      //  console.log('user .props.isAuthenticated', this.props.isAuthenticated);
      // console.log('user this.props.cartOrder', this.props.cartOrder);
      // let formIsValid = this.state.formIsValid && this.props.isAuthenticated && (this.props.cartOrder!==null);
      // this.setState({formIsValid: formIsValid}); 
        
    }

    inputChangedHandler = (event, inputIdentifier) => {
        
        const updatedFormElement = updateObject(this.state.orderForm[inputIdentifier], {
            value: event.target.value,
            valid: checkValidity(event.target.value, this.state.orderForm[inputIdentifier].validation),
            touched: true
        });
        const updatedOrderForm = updateObject(this.state.orderForm, {
            [inputIdentifier]: updatedFormElement
        });
        
        let formIsValid = true;
        for (let inputIdentifier in updatedOrderForm) {
            formIsValid = updatedOrderForm[inputIdentifier].valid 
            && formIsValid && this.props.isAuthenticated && this.props.cartOrder!==null;
        }
     //   console.log('user change implemented');
        this.setState({orderForm: updatedOrderForm, formIsValid: formIsValid});
    }

    // isFormValid = () => {
    //     return( this.state.formIsValid)
    // }

    render () {
        const formElementsArray = [];
        for (let key in this.state.orderForm) {
            formElementsArray.push({
                id: key,
                config: this.state.orderForm[key]
            });
        }
        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        label={formElement.config.elementConfig.placeholder}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <button type="submit" className="success" id="success"
                    disabled={!((this.state.formIsValid) && (this.props.cartOrder!==null))}>
                    Submit my Detail and my Order
                </button>
                <style jsx>
            {`
                button#success{
                    margin: 10px 10px 30px 10px!important;
                    background-color: #eeeeff!important;
                    border: none!important;
                    color: darkblue!important;
                    outline: none!important;
                    cursor: pointer!important;
                    font: inherit!important;
                    padding: 10px!important;
                    font-weight: bold!important;
                    border-radius: 10px!important;
                    border: 1px solid grey!important;
                }
                #success[disabled], button[disabled]{
                    cursor: not-allowed!important;
                }
                
                button#success:hover {
                    background: rgba(0,0,0,0)!important;
                    color: #3a7999!important;
                    box-shadow: inset 0 0 0 3px #3a7999!important;
                }
            `}
            </style>
            </form>
        );
        if ( this.props.loading ) {
            form = <Spinner />;
        }
        return (
            
            <div className="ContactData">
                <h4>Enter Your User Details</h4>
                {form}
            
            
            <style jsx >
            {`
                .ContactData{
                    text-align: center;
                }

            `}
            </style>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        cartOrder: state.floatCart.cartOrder,
        token: state.auth.token,
        userId: state.auth.userId,
        isAuthenticated: state.auth.token !== null,
        userDetails: state.auth.userDetails
    }
};

const mapDispatchToProps = dispatch => {
    return {
    addUserDetails: (userDetails) => dispatch(actions.addUserDetails(userDetails)),
    onGetUserDetails: (userId) => dispatch(actions.getUserDetails(userId)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserDetail);