import React, { Component } from 'react';
import { connect } from 'react-redux';
import Spinner from './UI/spinner/spinner';

import * as actions from '../store/actions/index';
import Auxiliry from '../hoc/Auxiliry/Auxiliry';
import Moment from 'moment';


class Orders extends Component {
    initialState = {
        currentPage: 1, 
        ordersPerPage: 10,
        formIsValid: false,
        productData: {},
        productAdded: false,
        productId: 0
    };

    state = {...this.initialState}

    componentWillReceiveProps = (nextProps) => {
     //   console.log('WillReceive this.props.userId', this.props.userId)
     //   console.log('WillReceive props.orders',this.props.orders);
      //  console.log('WillReceive orders_loading',this.props.orders_loading);
      //console.log('WillReceive props.userId',this.props.userId);
      let userId = localStorage.getItem('userId');
      console.log('WillReceive userId',userId===this.props.userId);
      if(this.props.userId  && this.props.orders_loading){
        this.props.onFetchOrders(this.props.userId);
          //  this.setState({productAdded: nextProps.productAdded})
        }

    };

    componentDidMount() {
        // AJAX CALL
        let userId = localStorage.getItem('userId');
       // console.log('did forders userid',userId);
        this.props.onFetchOrders(userId);
       //console.log('DidMount userId',userId);
   }

// componentWillReceiveProps(nextProps, nextState) {
//     if( nextProps.orders !== this.props.orders){
//         // make a new filteredProduct
//         let userId = localStorage.getItem('userId');
//         console.log('will forders userid',userId);
//         this.props.onFetchOrders(userId);
//     }
// }

   sortByDate(p_arr) {
       if (p_arr.length >= 2 ){
            p_arr = p_arr.sort((a, b) => {
                if (a.date < b.date) return -1;
                if (a.date > b.date) return 1;
                return 0;
            });
        }
    return p_arr;
}

handleClick = (event) => {
    this.setState({
        currentPage: Number(event.target.id)
    });
}

openPattern = (orderID) => {

}

render () {
        let renderPageNumbers = '';
        const { currentPage, ordersPerPage  } = this.state; 
        let currentOrders = [];
        let sortedOrders= [];
        let orders = <Spinner />;
        //console.log('this.props. forders',this.props.orders !== null);
        //console.log('orders_loading forders',this.props.orders_loading);
        //console.log('forders this.props.isAuthenticated === true',this.props.isAuthenticated===true);
        //console.log('forders orders_loading === false',this.props.orders_loading ===false);
        //console.log('orders_loading b forders',(!this.props.orders_loading && this.props.orders !== null));
    if (this.props.isAuthenticated === true && this.props.orders_loading === false && this.props.orders !== null ){ 
            sortedOrders = this.sortByDate(this.props.orders);
            
            const indexOfLastOrder = (sortedOrders.length < ordersPerPage)? sortedOrders.length  : currentPage * ordersPerPage;
            const indexOfFirstOrder = (sortedOrders.length < ordersPerPage)? 0 : indexOfLastOrder - ordersPerPage;
            currentOrders = sortedOrders.slice(indexOfFirstOrder, indexOfLastOrder);
           
            if(this.props.isAuthenticated===true){
                orders = (
                    <Auxiliry>
                    <table className="table table-default myTable">
                        <thead>
                            <tr>
                                <th >Order Date</th>
                                <th >Product Qty</th>
                                <th>Sub Total</th>
                                <th>Shipping</th>
                                {/* <th>Tax</th> */}
                                <th>Total Price</th>
                            </tr>
                        </thead>
                    </table>
                    <div >
                    {currentOrders.map( order => {
                        return (
                            <div key={order.order_id}>
                        <div id="orderData" className="orderFlexTable "  
                            onClick={() => this.openPattern(order.order_id)}>
                            <span className="ordercell">{Moment(order.date).format('DD/MM/YYYY')} </span>
                            <span className="ordercell">{order.cartTotals.total_qty}</span>
                            <span className="ordercell">{order.cartTotals.sub_total}</span>
                            <span className="ordercell">{order.cartTotals.shipping}</span>
                            {/* <span className="orderCell">{order.cartTotals.tax}</span> */}
                            <span className="ordercell">{order.cartTotals.totalPrice}</span>
                        </div>
                            {/* {order.cartProducts.map( prd => {
                                return (
                                <div className="panel" key={prd.product_id} width="100%">
                                <span className="orderCell">{prd.pattern_id}</span>
                                <span className="orderCell">in {prd.color} color</span>
                                </div>
                            )})} */}
                            </div>
                    )})}
                    </div>
                    </Auxiliry>
                 )
             } ;

         // Logic for displaying page numbers
         const pageNumbers = [];
         for (let i = 1; i <= Math.ceil(sortedOrders.length / ordersPerPage); i++) {
           pageNumbers.push(i);
         }
 
         renderPageNumbers = pageNumbers.map(number => {
           return (
             <li className="mypage-item"  key={number} id={number} 
                 ><span className="page-btn" onClick={this.handleClick} id={number}>
             {number}
             </span></li>
           );
         });
    }

        const orders_height = '450px';

        return (
            <div >
                <h4 className="text-center">Your Orders</h4>
                <div className='ordersBox'>
                {orders}
                </div>
                <div className="my-pager"> 
                    <nav aria-label="..." >
                        <ul className="pagination pagination-lg">   
                        {renderPageNumbers}
                        </ul>
                    </nav>
                </div>
            <style jsx global>{`
            .accordion{
                cursor: pointer;
            }
            .active, .accordion:hover {
                background-color: #ccc;
            }
            .panel {
                padding: 0 18px;
                background-color: white;
                display: none;
                overflow: hidden;
            }
            .orderFlexTable{
                display: flex;
                justify-content: space-between;
                align-items: baseline;
                align-content: flex-start;
                flex-flow: row nowrap;
                width: 100%;
                text-align: left;
            }
            
            #orderData > span,
            .ordercell{
                min-width: 45px;
                margin: 0 0 0 4vw;
                padding: 0px;
                font-size: 14px;
                text-align: right;
            }
            #orderData > span:nth-child(1){
                margin-left: 0!important;
                padding-left: 0px; 
                width: 75px!important;
            }
            
            // #orderData > span:nth-child(2){
            //     width: 25%!important;
            //     max-width: 200px;
            // }
            // #orderData > span:nth-child(3){
            //     width: 15%;
            // }
            // #orderData > span:nth-child(4){
            //     width: 15%;
            // }
            // #orderData > span:nth-child(5){
            //     width: 20%;
            // }

            .text-center{
                text-align: center;
                font-size:24px!important;
            }
            .ordersBox {
                color: black!important;
                margin: 0px auto 0px auto;
                width: 90%;
                height: 80%;
                text-align: center;
                border: none;
                padding: 30px auto;
                box-sizing: border-box;
            }
            
            @media (max-width: 460px) {
                .ordersBox {
                    width: 100%;
                    padding: 3px;
                }
                #orderData > span{
                    font-size: 9px;
                }
                #orderData > .orderCell{
                    width: auto;
                    margin: 5px;
                    padding: 0px;
                }
                div.ordersBox > div > div > span{
                    margin-left: 5px;
                    margin-right: 0px;
                }

            }
            .ordersBox > table,
            .ordersBox > .table, .ordersBox > .myTable{
                margin: 0px!important;
                margin-bottom: 0px!important;
            }
             .ordersBox > table > thead > tr > th{
                padding: 0px
                text-align:center;
            }
            
            .ordersBox > table > thead > tr > th:nth-child(5){
                text-align:right;
            }

            div.my-pager{
                text-align: center;
            }

            `}
            </style>
            </div>
        );
    }
}

// 
const mapStateToProps = state => {
    return {
        orders: state.products.orders,
        orders_loading: state.products.orders_loading,
        token: state.auth.token,
        isAuthenticated: state.auth.token !== null,
        userId: state.auth.userId,
        isAdmin: state.auth.isAdmin
    }
};
// product, this.props.token
const mapDispatchToProps = dispatch => {
    return {
        onFetchOrders: (userId) => dispatch(actions.fetchOrders(userId)),
        onOrdersClose: () => dispatch(actions.ordersClose())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Orders);