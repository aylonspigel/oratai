import React from 'react';
import Link from 'next/link';

export default ({ pathname }) => (

<div className='container-fluid ourinfo'>
  <div className="col-sm-6" id="our-data">
        <h1>Contact Us</h1>
        <hr/>
        <h1 >www.orataiphathai.com</h1>
        <br/>
        <h2>Wholesale, retail of Fashaionable Sarong</h2>
        <h2>Can Wear Every Day!</h2>
        <br/>
        <br/>
        <div className="img-box"><img src="../static/store_front.jpg" className="img-responsive" width="400px" height="300px" alt="Fashionable Thai Sarong Fabric" />
        </div>
        <br/>
        <p>Open Hours: daily 9:00 AM - 06:00 PM</p>

        <div className="media-pair">
          <img src="../static/phone.png" width="40px" className="img-icon" alt="orataiphathai Thai Sarong phone" />
          <p className="info-text">084-1200065</p>
        </div>
        <div className="media-pair">
          <img src="../static/mail.png" width="40px"  className="img-icon" alt="orataiphathai Thai Sarong Email" />
          <a href="mailto:orataiphathai@gmail.com" className="info-text">
        orataiphathai@gmail.com</a>
        </div>
        <div className="media-pair">
          <img src="../static/line.gif" width="40px" className="img-icon" alt="orataiphathai on LINE Thai Sarong" />
          <a href="line://ti/p/@bcj6324n" className="info-text" target="_new" >Add Orataiphathai on LINE</a>
        </div>
        <div className="media-pair">
          <img src="../static/facebook_icon.png" width="40px"  className="img-icon" alt="orataiphathai Email Thai Sarong" />
          <a href="https://www.facebook.com/%E0%B8%AD%E0%B8%A3%E0%B8%97%E0%B8%B1%E0%B8%A2%E0%B8%9C%E0%B9%89%E0%B8%B2%E0%B9%84%E0%B8%97%E0%B8%A2-937874942959104/" 
          target="_new" className="info-text">
          Orataiphathai - Facebook Page</a>
        </div>
  </div>
  <div className="col-sm-6 ">
            <div id="our-location">
                <h1>Location</h1>
                <hr/>
                <div className="media-pair">
                  <img src="../static/store.png" width="40px" className="img-icon" alt="Thai Sarong - orataiphathai address" />
                  <p className="info-text">30/147 ซอย บางแก้ว 2 Tambon Bang Kaeo, Bang Phli, Samut Prakan 10540, Thailand</p>
                </div>
                {/* gmap  */}
                <div className="embed-responsive embed-responsive-16by9">
                <iframe className="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15508.609535971156!2d100.6735125!3d13.6484929!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x42b66725ddc7bcc7!2z4Lij4LmJ4Liy4LiZ4Lit4Lij4LiX4Lix4Lii4Lic4LmJ4Liy4LmE4LiX4Lii!5e0!3m2!1sen!2sth!4v1538150428457" 
                width="100%" height="400" frameBorder="0" style={{ border:0 }} allowFullScreen></iframe>
            </div>
                
            <div className="media-pair">
              <img src="../static/line.gif" width="40px" className="img-responsive img-icon" alt="Thai Sarong Orataiphathai on LINE QR code" />
              <p className="info-text">Add Oratai Phathai on LINE <br/>with this BARCODE</p>
            </div>
         <img src="../static/LINE_QR_CODE.png" width="400px" className="img-responsive" alt="Thai Sarong Orataiphathai on LINE QR code" />

            </div>
        </div>
      <style jsx>{`
        .ourinfo {
          display: block;
          margin: 0px;
          padding: 20px;
          background-color: #000000!important;
          color: white;
        }
        .ourinfo > a{
          background-color: #000000!important;
          
        }
        #our-location > h1 ,
        #our-data > h1 {
          font-size: 24px;
          background-color: #000000!important; 
        }
        .img-box{
          width: 100%;
          padding: 0 auto;
          margin: 0;
        }
        #our-location > p,
        #our-data > p{
          font-size: 18px;
        }

        .media-pair{
          display: flex;
          justify-content: flex-start;
          align-items: center;
        }
        h2 {
          font-size: 20px;
          margin: 0 0 5px 0;
          text-align: center;
        }
        .img-icon{
          order: 1;
          width: 50px;
          margin: 5px 10px 5px 0;
          vertical-align: middle;
        }
        .info-text{
          order: 2;
          margin: 0px;
          font-size: 18px;
          vertical-align: middle;
          margin: auto 0;
          background-color: #000000!important;
          color: white;
        }
        #our-data > div > a:focus,
        #our-data > div > a:hover,
        a:focus,  a:hover {
          text-decoration: none!important;
          color: yellow;
          cursor: pointer;
        }
      `}</style>
      </div>
)