import PropTypes from 'prop-types'

export default WrappedComponent => {
  class WithProps extends React.Component {
    static async getInitialProps({ pathname, req }) {
      return { pathname: req && req.url || pathname  }
    }
    getChildContext() {
      return { pathname: this.props.pathname }
    }
    render() {
      return <WrappedComponent />
    }
  }
  WithProps.childContextTypes = {
    pathname: PropTypes.string.isRequired
  }

  return WithProps
}