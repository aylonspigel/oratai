import App, {Container} from 'next/app';
import React from 'react';
import withReduxStore from '../lib/with-redux-store';
import { Provider } from 'react-redux';

class MyApp extends App {
  render () {
     const {Component, pageProps, reduxStore} = this.props; 
    return (
      <Container>
        <Provider store={reduxStore}>
        <div className="App">
          <Component {...pageProps} />
          <style jsx global>
          {`

main {
  padding: 0px;
  max-width: 1600px;
  margin: 0px auto 0 auto; }

.clearfix {
  clear: both; }

/* MAC scrollbar para desktop*/
@media screen and (min-width: 640px) {
  .float-cart__content::-webkit-scrollbar {
    -webkit-appearance: none;
    width: 10px;
    background-color: rgba(0, 0, 0, 0.2);
    padding: 10px; }
  .float-cart__content::-webkit-scrollbar-thumb {
    border-radius: 4px;
    background-color: #0c0b10; } }

.float-cart__content {
  height: 100%;
  overflow-y: scroll; }

.main-banner {
  width: 100%; }



.filters {
  width: 15%;
  float: left; }
  .filters .star-button-container {
    text-align: center; }
    .filters .star-button-container small {
      color: #aaa;
      margin-bottom: 8px;
      display: inline-block; }
  .filters .title {
    margin-top: 2px;
    margin-bottom: 20px; }
  .filters-available-size {
    display: inline-block;
    margin-bottom: 10px;
    /* Customize the label (the container) */ }
    .filters-available-size label {
      display: inline-block;
      position: relative;
      cursor: pointer;
      font-size: 22px;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      width: 35px;
      height: 35px;
      font-size: 0.8em;
      margin-bottom: 8px;
      margin-right: 8px;
      border-radius: 50%;
      line-height: 35px;
      text-align: center;
      /* On mouse-over, add a grey background color */
      /* When the checkbox is checked, add a blue background */
      /* Show the checkmark when checked */
      /* Create a custom checkbox */ }
      .filters-available-size label:hover input ~ .checkmark {
        border: 1px solid #1b1a20; }
      .filters-available-size label input:checked ~ .checkmark {
        background-color: #1b1a20;
        color: #ececec; }
      .filters-available-size label input:checked ~ .checkmark:after {
        display: block; }
      .filters-available-size label input {
        position: absolute;
        opacity: 0;
        cursor: pointer; }
      .filters-available-size label .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        width: 35px;
        height: 35px;
        font-size: 0.8em;
        border-radius: 50%;
        line-height: 35px;
        text-align: center;
        color: #1b1a20;
        background-color: #ececec;
        border: 1px solid transparent; }

.shelf-container {
  width: 85%;
  float: left;
  min-height: 600px; }
  .shelf-container-header {
    margin-bottom: 10px; }
    .shelf-container-header .products-found {
      float: left;
      margin: 0;
      margin-top: 8px; }
    .shelf-container-header .sort {
      float: right; }
      .shelf-container-header .sort select {
        background-color: #fff;
        outline: none;
        border: 1px solid #ececec;
        border-radius: 2px;
        margin-left: 10px;
        width: auto;
        height: 35px;
        cursor: pointer; }
        .shelf-container-header .sort select:hover {
          border: 1px solid #5b5a5e; }
  .shelf-container .shelf-item {
    float: left;
    width: 25%;
    position: relative;
    text-align: center;
    box-sizing: border-box;
    padding: 5px;
    margin-bottom: 10px;
    border: 1px solid transparent; }
    .shelf-container .shelf-item:hover {
      border: 1px solid #eee; }
      .shelf-container .shelf-item:hover .shelf-item__buy-btn {
        background-color: #eabf00; }
    .shelf-container .shelf-item .shelf-stopper {
      position: absolute;
      color: #ececec;
      top: 10px;
      right: 10px;
      padding: 5px;
      font-size: 0.6em;
      background-color: #1b1a20;
      cursor: default; }
    .shelf-container .shelf-item__thumb img {
      width: 100%; }
    .shelf-container .shelf-item__title {
      position: relative;
      padding: 0 20px;
      height: 45px; }
      .shelf-container .shelf-item__title::before {
        content: '';
        width: 20px;
        height: 2px;
        background-color: #eabf00;
        position: absolute;
        bottom: 0;
        left: 50%;
        margin-left: -10px; }
    .shelf-container .shelf-item__price {
      height: 60px; }
      .shelf-container .shelf-item__price .val b {
        font-size: 1.5em;
        margin-left: 5px; }
      .shelf-container .shelf-item__price .installment {
        color: #9c9b9b; }
    .shelf-container .shelf-item__buy-btn {
      background-color: #1b1a20;
      color: #fff;
      padding: 15px 0;
      margin-top: 10px;
      cursor: pointer;
      transition: background-color 0.2s; }
//  #1b1a20;
.float-cart {
  position: fixed;
  top: 0;
  right: -750px;
  width: 750px;
  height: 100%;
  z-index: 600;
  background-color: white; 
  box-sizing: border-box;
  transition: right 0.2s; }

  .float-cart--open {
    right: 0; }
  .float-cart__close-btn {
    width: 50px;
    height: 50px;
    color: #666666;
    background-color: #ffffff;
    border: 4px solid #333333;
    text-align: center;
    line-height: 50px;
    position: absolute;
    top: 0;
    left: -50px;
    cursor: pointer; }
    .float-cart__close-btn:hover {
      background-color: #333333; }
  .float-cart .bag {
    width: 40px;
    height: 40px;
    position: relative;
    display: inline-block;
    vertical-align: middle;
    margin-right: 15px;
    background-image: url("./../static/bag-icon.png");
    background-repeat: no-repeat;
    background-size: contain;
    background-position: center; }
    .float-cart .bag--float-cart-closed {
      position: absolute;
      background-color: #000;
      border: 2px solid white;
      background-size: 50%;
      left: -60px;
      width: 60px;
      height: 60px;
      cursor: pointer; }
      .float-cart .bag--float-cart-closed .bag__quantity {
        bottom: 5px;
        right: 10px; }
    .float-cart .bag__quantity {
      display: inline-block;
      width: 18px;
      height: 18px;
      color: #0c0b10;
      font-weight: bold;
      font-size: 0.7em;
      text-align: center;
      line-height: 18px;
      border-radius: 50%;
      background-color: #eabf00;
      position: absolute;
      bottom: -5px;
      right: 0px; }
  .float-cart__header {
    color: #000000;
    box-sizing: border-box;
    text-align: center;
    padding: 45px 0; }
    .float-cart__header .header-title {
      
      font-weight: bold;
      font-size: 1.2em;
      vertical-align: middle; }
  .float-cart__shelf-container {
    min-height: 280px; }
    .float-cart__shelf-container .shelf-empty {
      color: #ececec;
      text-align: center;
      line-height: 40px; }
    .float-cart__shelf-container .shelf-item {
      position: relative;
      box-sizing: border-box;
      padding: 5%;
      transition: background-color 0.2s, opacity 0.2s; }
      .float-cart__shelf-container .shelf-item::before {
        content: '';
        width: 90%;
        height: 2px;
        background-color: rgba(0, 0, 0, 0.2);
        position: absolute;
        top: 0;
        left: 5%; }
      .float-cart__shelf-container .shelf-item--mouseover {
        background: #0c0b10; }
        .float-cart__shelf-container .shelf-item--mouseover .shelf-item__details .title, .float-cart__shelf-container .shelf-item--mouseover .shelf-item__details .desc {
          text-decoration: line-through;
          opacity: 0.6; }
        .float-cart__shelf-container .shelf-item--mouseover .shelf-item__price {
          text-decoration: line-through;
          opacity: 0.6; }
      .float-cart__shelf-container .shelf-item__del {
        width: 16px;
        height: 16px;
        top: 15px;
        right: 5%;
        border-radius: 50%;
        position: absolute;
        background-size: auto 100%;
        background-image: url("./../static/sprite_delete-icon.png");
        background-repeat: no-repeat;
        z-index: 2;
        cursor: pointer; }
        .float-cart__shelf-container .shelf-item__del:hover {
          background-position-x: -17px; }
      .float-cart__shelf-container .shelf-item__thumb, .float-cart__shelf-container .shelf-item__details, .float-cart__shelf-container .shelf-item__price {
        display: inline-block;
        vertical-align: middle; }
      .float-cart__shelf-container .shelf-item__thumb {
        vertical-align: middle;
        width: 15%;
        margin-right: 3%; }
        .float-cart__shelf-container .shelf-item__thumb img {
          width: 100%;
          height: auto; }
      .float-cart__shelf-container .shelf-item__details {
        width: 57%; 
      color: black}
        .float-cart__shelf-container .shelf-item__details .title {
          color: #ececec;
          margin: 0; }
        .float-cart__shelf-container .shelf-item__details .desc {
          color: #000000;
          margin: 0; }
      .float-cart__shelf-container .shelf-item__price {
        color: green;
        text-align: right;
        width: 25%; }
  .float-cart__footer {
    box-sizing: border-box;
    padding: 5%;
    box-shadow: 0px -0.5em 1em rgba(0, 0, 0, 0.3); }
    .float-cart__footer .sub, .float-cart__footer .sub-price {
      color: red;
      vertical-align: middle;
      display: inline-block; }
    .float-cart__footer .sub {
      width: 20%; }
    .float-cart__footer .sub-price {
      width: 80%;
      text-align: right; }
      .float-cart__footer .sub-price__val, .float-cart__footer .sub-price__installment {
        margin: 0; }
      .float-cart__footer .sub-price__val {
        color: orange;
        font-size: 22px; }
        .float-cart__footer .total-price {
          color: orange;
          font-size: 22px; }
        
    .float-cart__footer .buy-btn {
      color: #ececec;
      text-transform: uppercase;
      background-color: #0c0b10;
      text-align: center;
      padding: 15px 0;
      margin-top: 40px;
      cursor: pointer;
      transition: background-color 0.2s; }
      .float-cart__footer .buy-btn:hover {
        background-color: #000; }

@media only screen and (max-width: 1024px) {
  .filters {
    width: 20%; }
  .shelf-container {
    width: 80%; }
    .shelf-container .shelf-item {
      width: 33.33%; } }

@media only screen and (max-width: 640px) {
  .filters {
    width: 25%; }
  .shelf-container {
    width: 75%; }
    .shelf-container .shelf-item {
      width: 50%;
      padding: 10px; }
      .shelf-container .shelf-item__title {
        margin-top: 5px;
        padding: 0; }
  .float-cart {
    width: 100%;
    right: -100%; }
    .float-cart--open {
      right: 0; }
    .float-cart__close-btn {
      left: 0px;
      z-index: 2;
      background-color: #1b1a20; }
    .float-cart__header {
      padding: 25px 0; } }

@media only screen and (max-width: 460px) {
 
  
.lds-ring {
  position: fixed;
  top: 50%;
  left: 50%;
  margin-left: -32px;
  margin-top: -32px;
  width: 64px;
  height: 64px;
  z-index: 10;
  border-radius: 5px;
  background-color: #000; }
  .lds-ring div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 51px;
    height: 51px;
    margin: 6px;
    border: 6px solid #fff;
    border-radius: 50%;
    animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: #fff transparent transparent transparent; }
    .lds-ring div:nth-child(1) {
      animation-delay: -0.45s; }
    .lds-ring div:nth-child(2) {
      animation-delay: -0.3s; }
    .lds-ring div:nth-child(3) {
      animation-delay: -0.15s; }

@keyframes lds-ring {
  0% {
    transform: rotate(0deg); }
  100% {
    transform: rotate(360deg); 
    } 
}

.colorsClusterBox{
  display: flex;
  justify-content: flex-start; 
  flex-wrap: wrap;
}
.colorsCluster{
  flex: 0 1 auto ;
  height: 1em;
margin-bottom: 5px;

}
`}
        </style>
        </div>
        </Provider>
      </Container>
    );
  };
};

export default withReduxStore(MyApp);
