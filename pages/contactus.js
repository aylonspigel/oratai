import App from '../components/App'
import ContactUs from '../components/ContactUs'
export default () => (
  <App title="Contact Orataiphathai Thai Sarong"
  description="How to get the most out of our website and contact us with any question about our Thai Sarong." 
  keywords="Contact us"
  >
      <ContactUs />
  </App>
)