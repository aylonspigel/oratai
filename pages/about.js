import App from '../components/App'
import AboutUs from '../components/AboutUs'


export default () => (
  <App title="About Orataiphathai Thai Sarong" 
  description="Everything you need to know about OraTai Thai Sarong website: how to get the most out of our website or contact us with any question." 
  keywords="about us" >
    <AboutUs />
  </App>
)
