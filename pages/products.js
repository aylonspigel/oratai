import React from 'react';
import App from '../components/App';
import BasicProducts from '../components/basicProducts'

 export default () => (
  <App title="Order Your Orataiphathai Thai Sarong Products"
  description="Our Products catalog of Thai Sarong and more. You can browse our products and contact us for more information. We have many unique hand made items therefore you need to contact us to find out if the item is still avaviable." 
  keywords="Thai Sarong, Shopping cart, order products, products price, order products"
  >
    <BasicProducts  />
  </App>
);