import App from '../components/App'
import Tips from '../components/Tips'
export default () => (
  <App title="Tips by Orataiphathai "
  description="Tips by Orataiphathai about Sarong." 
  keywords="tips"
  >
      <Tips />
  </App>
)