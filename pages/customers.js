import App from '../components/App'
import Customers from '../components/Customers'
export default () => (
  <App title="Our Customer Gallery"
  description="A gallery of Our Customers and Thai Sarong fabric uses." 
  keywords="customers"
  >
      <Customers />
  </App>
)