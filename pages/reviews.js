import App from '../components/App'
import Reviews from '../components/Reviews'
export default () => (
  <App title="Customer Reviews of Orataiphathai Thai Sarong"
  description="Customer Reviews of Orataiphathai Great Service of Thai Sarong." 
  keywords="reviews"
  >
      <Reviews />
  </App>
)