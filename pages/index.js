import React, { Component } from 'react';
import App from '../components/App';
import Homepage from '../components/Homepage'

// export default () => (
//   <App title="Ora Tai Pha Thai Home"
//   description="Ora Tai Pha Thai home page. Our door for your pleasant shopping experience 
//   of Thai Sarong and fashionable fabric which you can wear every day." 
//   keywords="homepage, home page" >
//     <Homepage />
//   </App>
// )


class Index extends Component {
  // componentDidMount = () => {
  //     if ("serviceWorker" in navigator) {
  //         navigator.serviceWorker.register("/sw.js")
  //             .catch(err => console.error("Service worker registration failed", err));
  //     } else {
  //         console.log("Service worker not supported");
  //     }
  // }

  render() {
      return (
      <App title="Orataiphathai Thai Sarong Home"
        description="Orataiphathai Thai Sarong home page. Our door for your pleasant shopping experience 
        of Thai Sarong and fashionable fabric which you can wear every day." 
        keywords="Thai Sarong, Thai Sarong slideshow, Thai Sarong shop" 
        url="http://www.orataiphathai.com">
          <Homepage />
        </App>
      )
  }
}

export default Index;