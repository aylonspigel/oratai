const persistentCart = () => {
  const key = 'cartOrder';
  
  let localCart = localStorage.getItem(key);
  localCart = {
    "cartProducts": [
      {
          "product_id": "6303", 
          "pattern_id": "6303", 
          "color": "red", 
          "retail_price":222, 
          "wholesale_price":115, 
          "qty": 1
      },
      {
        "product_id": "6303", 
        "pattern_id": "6303", 
        "color": "yellow", 
        "retail_price":222, 
        "wholesale_price":115, 
        "qty": 1
    }
  ],
  cartTotals: {
    total_qty: 0,
    sub_total: 0,
    shipping: 0,
    procent_tax: "0%",
    totalPrice: 0,
    currencyId: "THB",
    currencyFormat: '฿'
  }
  }

  return {
    persist: (data) => localStorage.setItem(key, data),
    get: () => JSON.stringify(localCart),
  }

}

export default persistentCart;

// localStorage.getItem(key) ||'{}'