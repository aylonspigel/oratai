import { UPDATE_CART} from './actionTypes';

import persistentCart from '../persistentCart';

// const sumup = (accumulator, currentValue) => accumulator + currentValue;


export const updateCart = (cartProducts) => dispatch => {

  //let cartProducts = cartOrder.cartProducts;
  let totalQuantity = cartProducts.reduce((sum, p) => {
    sum += p.qty;
    return sum;
  }, 0);
// add pattern price  variety
  let subTotal = cartProducts.reduce((sum, p) => {
    if(totalQuantity<=11){
       sum += p.retail_price * p.qty
     }else{
      sum += p.wholesale_price * p.qty
     }
    return sum;
  }, 0);

  let shipping = cartProducts.reduce((steps, p) => {
      let shipPrice = (p.qty <= 6) ? 50 : (p.qty <=11)? 75: 100;
    return shipPrice;
  }, 0);

  let tax = cartProducts.reduce((steps, p) => {
    let taxPrice = subTotal * 7/100;
  return taxPrice;
}, 0);

let totalPrice = subTotal + tax + shipping;
  
  let cartTotals = {
    "total_qty": totalQuantity,
    "sub_total": subTotal,
    "shipping": shipping,
    "procent_tax": "0%",
    "totalPrice": totalPrice,
    "currencyId": "THB",
    currencyFormat: '฿'
  }
  let cartOrder = {
    "cartProducts":cartProducts,
    "cartTotals":cartTotals
  };
  persistentCart().persist(JSON.stringify(cartOrder)); // cartProducts

  dispatch({
    type: UPDATE_CART,
    cartOrder: cartOrder,
  });

}
