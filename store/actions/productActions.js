import { FETCH_SHIRTS } from './actionTypes';
import axios from 'axios';


const productsAPI = "https://react-shopping-cart-67954.firebaseio.com/products.json"
// https://oratai-2018.firebaseio.com/product.json";


const compare = {
  'lowestprice': (a, b) => {
    if (a.price < b.price)
      return -1;
    if (a.price > b.price)
      return 1;
    return 0;
  },
  'highestprice': (a, b) => {
    if (a.price > b.price)
      return -1;
    if (a.price < b.price)
      return 1;
    return 0;
  }
}

export const fetchShirts = (filters, sortBy, callback) => 
  dispatch => {

  axios.get(productsAPI)
    .then(res => {
      let { shirts } = res.data; 
/* 
      console.log('fetchShirts',shirts);
      if(!!filters && filters.length > 0){
        shirts = shirts.filter( p => filters.find( f => p.availableSizes.find( size => size === f ) ) )
      }

      if(!!sortBy){
        shirts = shirts.sort(compare[sortBy]);
      }

      if(!!callback) {
        callback();
      }
 */
      return dispatch({
        type: FETCH_SHIRTS,
        payload: shirts
      });

    })
    .catch(err => {
      console.log(err);
      throw new Error('Could not fetch shirts. Try again later.');
    });
}