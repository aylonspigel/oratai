import * as actionTypes from './actionTypes';
import axios from '../../axios-firebase';
import Router from 'next/router';


export const addProductFail = (error) => {
    console.log(error)
    return {
        type: actionTypes.ADD_PRODUCT_FAIL,
        error: error
    };
}

export const addProductStart = () => {
    return {
        type: actionTypes.ADD_PRODUCT_START,
        product: {},
        productAdded: false,
        productId: 0
    };
};

export const addProductSuccess = () => {
    return {
        type: actionTypes.ADD_PRODUCT_SUCCESS,
        productAdded: true,
        loading: false
    };
};
//productData, token
export const addProduct = (product) => dispatch => {
        dispatch(addProductStart());
        let token = localStorage.getItem("token");
        // ?auth=' + token + token,  rest/saving-data/products.json
        let product_id=product.productData.pattern_id
        axios.put(`/products/${product_id}.json?auth=${token}`, product)
            .then(response => {
                dispatch(addProductSuccess());
                dispatch(addClose());
                dispatch(fetchProducts());

            })
            .catch(error => {
                console.log(error);
                dispatch(addProductFail(error));
            });
    };


export const fetchProductsFail = (error) => {
    return {
        type: actionTypes.FETCH_PRODUCTS_FAIL,
        error: error
    };
};

export const fetchProductsStart = () => {
    return {
        type: actionTypes.FETCH_PRODUCTS_START,
        loading: true,
        products: []
    };
};

export const fetchProductsSuccess = (productsArr) => {
    // orderBy product_name
    return {
        type: actionTypes.FETCH_PRODUCTS_SUCCESS,
        loading: false,
        products: productsArr
    };
};


export const fetchProducts = () => 
     dispatch => {
        //dispatch(fetchProductsStart());
        axios.get('/products.json')
            .then(res => {
                console.log('res.data',res.data);
                const fetchedProducts = [] ;//res.data;
                for (let key in res.data) {
                    let product = {};
                    product.product_id = key;
                    product.productData = res.data[key].productData;
                    if(res.data[key].productStock){
                        product.productStock = res.data[key].productStock;
                    }else{
                        product.productStock = [1,1,1,1,1,1,1,1,1,1,1,1,1];
                    }
                    
                    fetchedProducts.push(product);
                }
                 console.log('fetchedProducts.productStock',JSON.stringify(fetchedProducts.productStock));
                //return dispatch(fetchProductsSuccess([...fetchedProducts]));
                return dispatch({
                    type: actionTypes.FETCH_PRODUCTS_SUCCESS,
                    loading: false,
                    products: [...fetchedProducts]
                  });
            })
            .catch(err => {
                dispatch(fetchProductsFail(err));
                console.log(err);
                throw new Error('Could not fetch. Try again later.');
            });
}

export const fetchOrdersStart = () => {
    return {
        type: actionTypes.FETCH_ORDERS_START,
        orders_loading: true,
        orders: []
    };
};

export const fetchOrders = (userId) =>  dispatch => {
        //let userId = localStorage.getItem('userId');
        console.log('forders userid',userId);
        dispatch(fetchOrdersStart());
        
        axios.get(`/users/${userId}/orders.json`)
            .then(res => {
                // console.log('forders res.data',res.data);
                const fetchedOrders = [] ;
                for (let key in res.data) {
                    let order = {};
                    order.order_id = key;
                    order.cartPatterns = res.data[key].cartPatterns;
                    order.cartProducts = res.data[key].cartProducts;
                    order.cartTotals = res.data[key].cartTotals;
                    //order.date = (res.data[key].date)?res.data[key].date:'someday';
                    order.date = res.data[key].date ;
                    fetchedOrders.push(order);
                }
                // console.log('fetched forders',fetchedOrders);
                //return dispatch(fetchProductsSuccess([...fetchedProducts]));
                return dispatch({
                    type: actionTypes.FETCH_ORDERS,
                    orders_loading: false,
                    orders: [...fetchedOrders]
                  });
            })
            .catch(err => {
                //dispatch(fetchProductsFail(err));
                console.log(err);
                throw new Error('Could not fetch. Try again later.');
            });
}

export const deleteProductFail = (error) => {
    return {
        type: actionTypes.DELETE_PRODUCT_FAIL,
        error: error
    };
};

export const deleteProductStart = () => {
    return {
        type: actionTypes.DELETE_PRODUCT_START
    };
};

export const deleteProduct = (id) => {
    return dispatch => {
        if (id) {
            dispatch(deleteProductStart());
            let token = localStorage.getItem("token");
            axios.delete('/products/' + id + '.json?auth='+token, null)
                .then(res => {
                    console.log('delete product success');
                    dispatch(fetchProducts());
                })
                .catch(err => {
                    console.log('delete product fail', err);
                    // dispatch(deleteProductFail(err));
                });
        } else {
            console.warn('no id - cannot delete all!');
        }
    };
};

export const addOpen = () => {
    return {
        type: actionTypes.ADD_OPEN,
        addShow: true,
    };
};

export const addClose = () => {
    return {
        type: actionTypes.ADD_CLOSE,
        addShow: false
    };
};

export const editStockOpen = (pIn) => {
    return {
        type: actionTypes.EDIT_STOCK_OPEN,
        editStockShow: true,
        p_in: pIn
    };
};

export const editStockClose = () => {
    return {
        type: actionTypes.EDIT_STOCK_CLOSE,
        editStockShow: false
    };
};

export const editOpen = (pIn) => {
    return {
        type: actionTypes.EDIT_OPEN,
        editShow: true,
        p_in: pIn
    };
};

export const editClose = () => {
    return {
        type: actionTypes.EDIT_CLOSE,
        editShow: false
    };
};

export const showOpen = (pIn) => {
    return {
        type: actionTypes.SHOW_OPEN,
        showShow: true,
        p_in: pIn
    };
};

export const showClose = () => {
    return {
        type: actionTypes.SHOW_CLOSE,
        showShow: false
    };
};

export const ordersOpen = () => {
    return {
        type: actionTypes.ORDERS_OPEN,
        ordersShow: true
    };
};

export const ordersClose = () => {
    return {
        type: actionTypes.ORDERS_CLOSE,
        ordersShow: false
    };
};

export const filterProductsArray = (filteredProducts) => {

    return {
        type: actionTypes.FILTER_PRODUCTS_ARRAY,
        filteredProducts: [...filteredProducts],
        filtered: true
    };
};

export const filterOff = () => {
    return {
        type: actionTypes.FILTER_OFF,
        filtered: false
    };
};
export const filterOn = () => {
    return {
        type: actionTypes.FILTER_ON,
        filtered: true
    };
};

export const editProductStockStart = () => {
    return {
        type: actionTypes.EDIT_PRODUCT_STOCK_START
    };
};

export const editProductStockSuccess = () => {
    return {
        type: actionTypes.EDIT_PRODUCT_STOCK_SUCCESS,
        loading: false
    };
};

export const editProductStockFail = (error) => {
    return {
        type: actionTypes.EDIT_PRODUCT_STOCK_FAIL,
        error: error
    };
};


export const editProductStart = () => {
    return {
        type: actionTypes.EDIT_PRODUCT_START
    };
};

export const editProductSuccess = () => {
    return {
        type: actionTypes.EDIT_PRODUCT_SUCCESS,
        loading: false
    };
};

export const editProductFail = (error) => {
    return {
        type: actionTypes.EDIT_PRODUCT_FAIL,
        error: error
    };
};

export const editProduct = (id, productData, productStock) => {
    // console.log('action id', id);
    console.log('action productStock', productStock);
    let token = localStorage.getItem("token");
    return dispatch => {
        dispatch(editProductStart());
        
        console.log('editProduct id', id);
        //  ?auth=' + token + token,  rest/saving-data/products.json
        axios.put('/products/' + id + '/productData.json?auth='+token, productData)
            .then(response => {
                // console.log('action Editproduct Success');
                dispatch(editProductSuccess());
                //dispatch(filterProductsArray( ) );
                dispatch(editProductStock(id, productStock));
                // dispatch( editClose());
                // dispatch(fetchProducts());
            })
            .catch(error => {
                console.log('axios edit error' + error);
                dispatch(editProductFail(error));
            });
    };
};


export const editProductStock = (id, productStock) => {
    // console.log('action id', id); 
    // console.log('action productData', productData);
    let token = localStorage.getItem("token");
    console.log('editProductStock ', productStock);
    console.log('editProductStock id', id);
    if (productStock===null) {
        productStock = {0: 1,1: 1,2: 1,3: 1,4: 1,5: 1,6: 1,7: 1,8: 1,9: 1,10: 1,11: 1,12: 1}
    }else{
        productStock ={...productStock};
    }
    return dispatch => {
        dispatch(editProductStart());
        //  ?auth=' + token + token,  rest/saving-data/products.json
        axios.put('/products/' + id + '/productStock.json?auth='+token, productStock)
            .then(response => {
                dispatch(editProductStockSuccess());
                dispatch(editClose());
                dispatch(fetchProducts());
            })
            .catch(error => {
                console.log('axios edit stock  error' + error);
                dispatch(editProductFail(error));
            });
    };
};