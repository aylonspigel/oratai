import * as actionTypes from './actionTypes';
import axios from '../../axios-firebase';



export const fetchAllProductsFail = (error) => {
    return {
        type: actionTypes.FETCH_ALLPRODUCTS_FAIL,
        error: error
    };
};

export const fetchAllProductsStart = () => {
    return {
        type: actionTypes.FETCH_ALLPRODUCTS_START,
        loading: true,
        allProducts: []
    };
};

export const fetchAllProductsSuccess = (productsArr) => {
    // orderBy product_name
    return {
        type: actionTypes.FETCH_ALLPRODUCTS_SUCCESS,
        loading: false,
        allProducts: productsArr 
    };
};

export const fetchAllProducts = () => 
  dispatch => {
    dispatch(fetchAllProductsStart());
    axios.get('/allProducts.json')
            .then(res => {
        //   console.log('res.data',res.data);
                const fetchedProducts = [] ;//res.data;
                for (let key in res.data) {
                    let product = {};
                    product.id = key;
                    product.title = res.data[key].title;
                    product.group_id = res.data[key].group_id;
                    product.description = res.data[key].description;
                    product.pattern_id = res.data[key].pattern_id;
                    product.photo_url = res.data[key].photo_url;
                    product.retail_price = res.data[key].retail_price;
                    product.wholesale_price = res.data[key].wholesale_price;
                    
                    fetchedProducts.push(product);
                }
               // console.log('fetchedProducts',JSON.stringify(fetchedProducts));
                // return dispatch(fetchProductsSuccess([...fetchedProducts]));
                   return dispatch({
                       type: actionTypes.FETCH_ALLPRODUCTS_SUCCESS,
                       loading: false,
                       allProducts: [...fetchedProducts]
                     });
            })
            .catch(err => {
                dispatch(fetchAllProductsFail(err));
                console.log(err);
                throw new Error('Could not fetch. Try again later.');
            });
}



export const addProduct2AllFail = (error) => {
    console.log(error)
    return {
        type: actionTypes.ADD_PRODUCT2ALL_FAIL,
        error: error
    };
}

export const addProduct2AllStart = () => {
    return {
        type: actionTypes.ADD_PRODUCT2ALL_START,
        product: {},
        productAdded: false,
        productId: 0
    };
};

export const addProduct2AllSuccess = () => {
    return {
        type: actionTypes.ADD_PRODUCT2ALL_SUCCESS,
        productAdded: true,
        loading: false
    };
};
//productData, token
export const addProduct2All = (product) => dispatch => {
        dispatch(addProduct2AllStart());
        let token = localStorage.getItem("token");
        // ?auth=' + token + token,  rest/saving-data/products.json
        //let product_id=product.id;
        axios.post(`/allProducts.json?auth=${token}`, product)
            .then(response => {
                dispatch(addProduct2AllSuccess());
                dispatch(add2AllClose());
                dispatch(fetchAllProducts());

            })
            .catch(error => {
                console.log(error);
                dispatch(addProduct2AllFail(error));
            });
    };

    export const add2AllOpen = () => {
        return {
            type: actionTypes.ADD2ALL_OPEN,
            add2AllShow: true,
        };
    };
    
    export const add2AllClose = () => {
        return {
            type: actionTypes.ADD2ALL_CLOSE,
            add2AllShow: false
        };
    };

    export const openProduct = () => {
        return {
            type: actionTypes.OPEN_PRODUCT,
            proShow: true
        };
    };
    
    export const closeProduct = () => {
        return {
            type: actionTypes.CLOSE_PRODUCT,
            proShow: false
        };
    };

    
    export const updateCurrentProductId = (pid) => {
        return {
            type: actionTypes.UPDATE_PROD_ID,
            currentProductId: pid
        };
    };

    export const openEdit = (pid) => {
        return  {
            type: actionTypes.OPEN_EDIT,
            currentProductId: pid,
            editShow: true
        };
    };
    
    export const closeEdit = () => {
        return {
            type: actionTypes.CLOSE_EDIT,
            editShow: false
        };
    };

export const delProductFail = (error) => {
    return {
        type: actionTypes.DEL_PRODUCT_FAIL,
        error: error
    };
};

export const delProductStart = () => {
    return {
        type: actionTypes.DEL_PRODUCT_START
    };
};

export const delProductSuccess = () => {
    return {
        type: actionTypes.DEL_PRODUCT_SUCCESS
    };
};

    export const delProduct = (id) => {
        return dispatch => {
            if (id) {
                dispatch(delProductStart());
                let token = localStorage.getItem("token");
                axios.delete('/allProducts/' + id + '.json?auth='+token, null)
                    .then(res => {
                        console.log('delete product success');
                        dispatch(delProductSuccess());
                        dispatch(fetchAllProducts());
                    })
                    .catch(err => {
                        console.log('delete product fail', err);
                        dispatch(delProductFail(err));
                    });
            } else {
                console.warn('no id - cannot delete all!');
            }
        };
    };

    
    export const editProd = (id,currentProduct) => {
        let token = localStorage.getItem("token");
        return dispatch => {
            axios.put('/allProducts/' + id + '.json?auth='+token, currentProduct)
                .then(response => {
                    dispatch(editProdSuccess());
                    dispatch( closeEdit());
                    // dispatch(fetchAllProducts());
                })
                .catch(error => {
                    console.log('axios edit error' + error);
                    dispatch(editProductFail(error));
                });
        };
    };
    
    
export const editProdFail = (error) => {
    return {
        type: actionTypes.EDIT_PROD_FAIL,
        error: error
    };
};

export const editProdSuccess = () => {
    return {
        type: actionTypes.EDIT_PROD_SUCCESS,
        
    };
}