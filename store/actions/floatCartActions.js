import * as actionTypes from './actionTypes';
  //import persistentCart from '../persistentCart';
import axios from '../../axios-firebase';
import { updateObject } from '../../shared/utility';

export const loadCart = (cartOrder) => dispatch => {
  //let cartOrder = this.state .cartOrder
  return (dispatch, getState) => {
    const state = getState();

    if (!state.cartOrder) {
      // fetch and dispatch

    }
   // console.log('this.props.cartOrder', cartOrder);
    return dispatch({
      type: actionTypes.LOAD_CART,
      cartOrder: cartOrder
    });
  }
};

export const addProductCart = (productData) => {
  return dispatch( {
    type: ADD_PRODUCT_CART,
    cartOrder: cartOrder
  });
}

export const removeProductCart = (productData) => {
  return dispatch( {
    type: REMOVE_PRODUCT_CART,
    cartOrder: cartOrder
  });
}


// export const sendOrder = (cartOrder) => dispatch => {
//   //let userId = localStorage.getItem('userId');
//  cartOrder = {};
//   return dispatch( {
//       type: actionTypes.SEND_ORDER,
//       cartOrder: cartOrder
//   });
// };

// export const save2Cart = (userId, cartOrder) => dispatch => {
//   //localStorage.setItem('userCart', );
 
//   return dispatch( {
//       type: actionTypes.SAVE2CART,
//       cartOrder: cartOrder
//   });
// };

export const cart2Order = (cartOrder,userDetails) => dispatch => {
  let userid = localStorage.getItem('userId');
  let token = localStorage.getItem("token");
//  let userDetails = JSON.parse(localStorage.getItem('userDetails'));
 // let cartOrder = JSON.parse(persistentCart().get());
     // dispatch( addUserStart() );
      // ?auth=' + token + token,  rest/saving-data/products.json
      if(userid){
        let today = new Intl.DateTimeFormat('en-US', {          
          year: 'numeric',           
          month: 'long',           
          day: '2-digit'         
        }).format();
      //  console.log("today",today);
        cartOrder = updateObject(cartOrder, {"date":today});
        axios.post(`/users/${userid}/orders.json?auth=${token}`, cartOrder )
              .then(response => {
                // send email with, order detail to nadea
              //  console.log('send cart to order with email and empty cart');
                // localStorage.setItem('userDetails','')
                  cartOrder = null;
                  dispatch(updateCart(null,null));
                dispatch({
                        type: actionTypes.CART2ORDER,
                        cartOrder: null
                });
              })
              .catch( error => {
                dispatch({
                      type: actionTypes.CART2ORDERFAIL,
                      error: error
                  });
              });

      //  axios.put( `/users/${userid}/userDetails.json?auth=${token}`, 
      //  userDetails )
      //     .then( response => {
              
      //               })
      //     .catch( error => {
      //       dispatch({
      //             type: actionTypes.CART2ORDERFAIL,
      //             error: error
      //         });
      //     } ); 

}else{ console.log("no user")}
}

export const cartOpen = () => {
  return {
      type: actionTypes.CART_OPEN,
      cartShow: true,
  };
};

export const cartClose = () => {
  return {
      type: actionTypes.CART_CLOSE,
      cartShow: false,
  };
};


export const updateCart = (cartProducts, patternArray) => dispatch => {
  let cartPatterns = patternArray;
  // building the cartTotals
  if (cartProducts) {
    let totalQuantity = cartProducts.reduce((sum, p) => {
      sum += parseInt(p.qty);
    //  console.log("sum totalQuantity", sum);
      return sum;
    }, 0);
    // add pattern price  variety
    let subTotal = cartProducts.reduce((sum, p) => {
      if (parseInt(totalQuantity) <= 11) {
        sum = parseInt(p.retail_price) * parseInt(totalQuantity)
      } else {
        sum = parseInt(p.wholesale_price) * parseInt(totalQuantity)
      }
    //  console.log("sum subTotal", sum);
      return sum;
    }, 0);

  //  console.log("totalQuantity", totalQuantity);
    // shipping
    let shipping=0;
    if(parseInt(totalQuantity)>0){
      let ship = parseInt(totalQuantity) % 12;
      let ship12 = (parseInt(totalQuantity) - ship) / 12;
      shipping = (ship > 0 && ship <= 3) ? 50 : (ship <= 7) ? 75 : 100;
      shipping += ship12 * 100;
    }


    // tax = 7%
    let tax = subTotal * 0 / 100;

    let totalPrice = subTotal + tax + shipping;

    let cartTotals = {
      "total_qty": totalQuantity,
      "sub_total": subTotal,
      "shipping": shipping,
      "tax": tax,
      "totalPrice": totalPrice,
      "currencyId": "THB",
      currencyFormat: '฿'
    }
    let cartOrder = {
      "cartProducts": cartProducts,
      "cartPatterns": cartPatterns,
      "cartTotals": cartTotals
    };
    //persistentCart().persist(JSON.stringify(cartOrder)); // cartProducts
  
  dispatch({
    type: actionTypes.UPDATE_CART,
    cartOrder: cartOrder,
  });
  }else{
    dispatch({
      type: actionTypes.UPDATE_CART,
      cartOrder: null
    });
  }

}
