import authAxios from 'axios';
import axios from '../../axios-firebase';
import * as actionTypes from './actionTypes';

export const authOpen = () => {
    return {
        type: actionTypes.AUTH_OPEN,
        authShow: true
    };
};

export const authClose = () => {
    console.log('auth_close');
    return {
        type: actionTypes.AUTH_CLOSE,
        authShow: false
    };
};

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authSuccess = (token, userId) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        idToken: token,
        userId: userId,
        isAdmin: false
    };
};



export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error.message
    };
};

export const authLogout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    
    return {
        type: actionTypes.AUTH_LOGOUT,
        idToken: null,
        userId: null,
        isAdmin: false,
        email: null
    };
};

export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(authLogout());
        }, expirationTime * 1000);
    };
};

export const auth = (email, password, isSignup) => {
    return dispatch => {
        dispatch(authStart());
        const authData = {
            email: email,
            password: password,
            returnSecureToken: true
        };
        let url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyDW7ozYaZ9Z8_6pqHnyeVIJFNgwEkKrD_A';
        if (!isSignup) {
            url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyDW7ozYaZ9Z8_6pqHnyeVIJFNgwEkKrD_A';
        }
        authAxios.post(url, authData)
            .then(response => {
                const expirationDate = new Date(new Date().getTime() + response.data.expiresIn * 1000);
                console.log('response.data',response.data);
                localStorage.setItem('token', response.data.idToken);
                localStorage.setItem('expirationDate', expirationDate);
                localStorage.setItem('userId', response.data.localId);

                dispatch(authSuccess(response.data.idToken, response.data.localId));
                if (isSignup) {
                    // set up new user is database
                    dispatch(addUser(email,response.data.localId));
                }
                dispatch(getisAdmin(email));

                dispatch(checkAuthTimeout(response.data.expiresIn));
                dispatch(authClose());
            })
            .catch(err => {
                dispatch(authFail(err.response.data.error));
            });
    };
};

export const addUser = ( email, userId ) => {
    return dispatch => {
        dispatch( addUserStart() );
        let token = localStorage.getItem("token");
        // ?auth=' + token + token,  rest/saving-data/products.json
         axios.put( `/users/${userId}/.json?auth=${token}`, {'email': email, 'isAdmin': 'false'} )
            .then( response => {
                console.log('addusee success');
                let user = {'email': email, 'isAdmin': 'false'}
                dispatch( addUserSuccess( user ) );
            } )
            .catch( error => {
                console.log(error);
                dispatch( addUserFail( error ) );
            } ); 
    };
};

export const getUserDetails = (userId) => {
    return dispatch => {
        axios.get( `/users/${userId}/userDetails/userDetails.json`)
            .then(res => {
                let userDetails = res.data;
             //   console.log('userDetails', userDetails);
                dispatch(getUserDetailsSuccess(userDetails));
            })
            .catch(err => {
             //   console.log('error userDetails', userDetails);
                dispatch(getUserDetailsFail(err));
            });
    };
}

export const getUserDetailsFail = (error) => {
    return {
        type: actionTypes.GETUSER_DETAILS_FAIL,
        error: error,
        userDetails: false
    };
};

export const getUserDetailsSuccess = (userDetails) => {
    return {
        type: actionTypes.GETUSER_DETAILS_SUCCESS,
        userDetails: userDetails
    };
};

export const addUserDetails = (userDetails) => {
    return dispatch => {
        dispatch( addUserDetailsStart() );
        let token = localStorage.getItem("token");
        let userId = localStorage.getItem("userId");
        //let userDetails = JSON.parse(localStorage.getItem("userDetails"));
        //userDetails = userDetails;
        // ?auth=' + token + token,  rest/saving-data/products.json
         axios.put( `/users/${userId}/userDetails.json?auth=${token}`,  userDetails )
            .then( response => {
             ///   console.log('adduserdetails success');
                let userDetails2 = {'userId': userId, 'userDetails': userDetails} 
                dispatch( addUserDetailsSuccess( userDetails ) );
            } )
            .catch( error => {
                console.log(error);
                dispatch( addUserDetailsFail( error ) );
            } ); 
    };
  };

  export const addUserDetailsStart = () => {
    return {
        type: actionTypes.ADDUSER_DETAILS_START,
        
    };
};

export const addUserDetailsSuccess = (userDetails) => {
    return {
        type: actionTypes.ADDUSER_DETAILS_SUCCESS,
        userDetails: userDetails
    };
};

export const addUserDetailsFail = () => {
    return {
        type: actionTypes.ADDUSER_DETAILS_FAIL,
        error: error
    };
};


export const addUserStart = () => {
    return {
        type: actionTypes.ADDUSER_START,
        email: null
    };
};

export const addUserSuccess = (email) => {
    return {
        type: actionTypes.ADDUSER_SUCCESS,
        email: email,
        isAdmin: false
    };
};

export const addUserFail = () => {
    return {
        type: actionTypes.ADDUSER_FAIL,
        error: error
    };
};

export const setAuthRedirectPath = (path) => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path: path
    };
};
// ?'orderBy'='email'&'equalTo'='" + email + "'"
export const getisAdmin = (email) => {
    return dispatch => {
        axios.get("/users.json")
            .then(res => {
                let isAdminData = '';
                for (let key in res.data) {
                     if ((res.data[key].email).toLowerCase() === email.toLowerCase()) {
                        isAdminData = res.data[key].isAdmin;
                        dispatch(isAdminSuccess(email, isAdminData));
                     };
                };
            })
            .catch(err => {
                dispatch(isAdminFail(err));
            });
    };
};


export const isAdminSuccess = (email, isAdmin) => {
    return {
        type: actionTypes.ISADMIN_SUCCESS,
        email: email,
        isAdmin: isAdmin
    };
};


export const isAdminFail = (error) => {
    return {
        type: actionTypes.ISADMIN_FAIL,
        error: error
    };
};

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(authLogout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate <= new Date()) {
                dispatch(authLogout());
            } else {
                const userId = localStorage.getItem('userId');
                dispatch(authSuccess(token, userId));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000 ));
            }   
        }
    };
};