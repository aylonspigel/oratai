import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';


const initialState = {
  cartShow: false,
  cartOrder: null
};

const updateCart = ( state, action ) => {
  return updateObject( state, { cartOrder: action.cartOrder } );
};

const cartOpen = ( state, action ) => {
  return updateObject( state, { cartShow: true } );
};
const cartClose = ( state, action ) => {
  return updateObject( state, { cartShow: false } );
};

const loadCart = ( state, action ) => {
 // console.log('load cart success: ');
  return updateObject( state, {
      cartOrder: action.cartOrder
  } );
};

const addProductCart = (state, action) => {
  return updateObject( state, {
    cartProducts:action.cartOrder.cartProducts})
}

const removeProductCart = (state, action) => {
  return updateObject( state, {
    cartOrder:action.product})
}
 
const cart2Order = (state, action) => {
  return updateObject( state, {
    cartOrder:action.cartOrder})
}


const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_CART:  return loadCart(state, action); 
    case actionTypes.ADD_PRODUCT_CART: return addProductCart(state, action);
    case actionTypes.REMOVE_PRODUCT_CART: return removeProductCart(state, action);
    case actionTypes.CART2ORDER: return cart2Order(state, action);
    case actionTypes.CART_OPEN: return cartOpen(state, action);
    case actionTypes.CART_CLOSE: return cartClose(state, action);
    case actionTypes.UPDATE_CART: return updateCart(state, action);

    default:
      return state;
  }
};
// return {...state, cartOrder: action.cartOrder };
// return { ...state, itemToRemove: Object.assign({}, action.product) };
export default reducer;
