import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
  cartOrder: {
    cartProducts: [{
      product_id: "0",
      pattern_id: "0",
      color_id: "0",
      color: "red",
      retail_price: 0,
      wholesale_price: 0,
      qty: 0
    }],
    cartTotals: {
      total_qty: 0,
      sub_total: 0,
      shipping: 0,
      procent_tax: "0%",
      totalPrice: 0,
      currencyId: "THB",
      currencyFormat: '฿'
    }
  }
};

const updateCart = ( state, action ) => {
  return updateObject( state, { cartOrder: action.cartOrder } );
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.UPDATE_CART:  return updateCart(state, action); 
    default:
    return state;
  }
}

export default reducer;

