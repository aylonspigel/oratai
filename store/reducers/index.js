import { combineReducers } from 'redux';
import productReducer from './productReducer';
import floatCartReducer from './floatCartReducer';
import updateCartReducer from './updateCartReducer';
import filterReducer from './filterReducer';
import sortReducer from './sortReducer';
import auth from './auth';
import products from './products';
import allProducts from './allProducts';


export default combineReducers({
  allProducts: allProducts,
  products: products,
  floatCart: floatCartReducer,
  filters: filterReducer,
  sort: sortReducer,
  auth: auth
});