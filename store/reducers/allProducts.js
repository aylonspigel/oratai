import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    loading: true,
    allProducts: [],
    productAdded: false,
    productDeleted: false,
    add2AllShow: false,
    proShow: false,
    editShow: false,
    currentProductId: null
};




const fetchAllProductsStart = ( state, action ) => {
    return updateObject( state, { loading: true } );
};

const fetchAllProductsSuccess = ( state, action ) => {
    return updateObject( state, {
        allProducts: action.allProducts,
        loading: false
    } );
};

const fetchAllProductsFail = ( state, action ) => {
    return updateObject( state, { loading: false, error: action.error } );
};
 
const addProduct2AllStart = ( state, action ) => {
    return updateObject( state, { productAdded: false, loading: true } );
};

const addProduct2AllSuccess = ( state, action ) => {
    return updateObject( state, {
        productAdded: true,
        loading: false
    } );
};

const addProduct2AllFail = ( state, action ) => {
  //  console.log('add product fail');
    return updateObject( state, { loading: false } );
};

const add2AllOpen = ( state, action ) => {
    return updateObject( state, { add2AllShow: true } );
};
const add2AllClose = ( state, action ) => {
    return updateObject( state, { add2AllShow: false } );
};

const openProduct = ( state, action ) => {
    return updateObject( state, { proShow: true } );
};
const closeProduct = ( state, action ) => {
    return updateObject( state, { proShow: false } );
};

const delProductStart = ( state, action ) => {
     return updateObject( state, { productDeleted: false } );
 };

 const delProductSuccess = ( state, action ) => {
    return updateObject( state, { productDeleted: true } );
};

const openEdit = ( state, action ) => {
     return updateObject( state, { editShow: true } );
 };

 const closeEdit = ( state, action ) => {
    return updateObject( state, { editShow: false } );
};


const editProdSuccess = ( state, action ) => {
    return updateObject( state, { productEdited: true } );
};

const editProdFail = ( state, action ) => {
    return updateObject( state, { error: action.error } );
};

const updateCurrentProductId = ( state, action ) => {
    return updateObject( state, { currentProductId: action.currentProductId } );
};


const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.FETCH_ALLPRODUCTS_START: return fetchAllProductsStart( state, action );
        case actionTypes.FETCH_ALLPRODUCTS_SUCCESS: return fetchAllProductsSuccess( state, action );
        case actionTypes.FETCH_ALLPRODUCTS_FAIL: return fetchAllProductsFail( state, action );

        case actionTypes.ADD_PRODUCT2ALL_START: return addProduct2AllStart( state, action );
        case actionTypes.ADD_PRODUCT2ALL_SUCCESS: return addProduct2AllSuccess( state, action );
        case actionTypes.ADD_PRODUCT2ALL_FAIL: return addProduct2AllFail( state, action );

        case actionTypes.ADD2ALL_OPEN: return add2AllOpen(state, action);
        case actionTypes.ADD2ALL_CLOSE: return add2AllClose(state, action);

        case actionTypes.OPEN_PRODUCT: return openProduct(state, action);
        case actionTypes.CLOSE_PRODUCT: return closeProduct(state, action);

        case actionTypes.DEL_PRODUCT_FAIL: return delProductFail(state, action);
        case actionTypes.DEL_PRODUCT_START: return delProductStart(state, action);
        case actionTypes.DEL_PRODUCT_SUCCESS: return delProductSuccess(state, action);

        case actionTypes.OPEN_EDIT: return openEdit(state, action);
        case actionTypes.CLOSE_EDIT: return closeEdit(state, action);

        case actionTypes.EDIT_PROD_SUCCESS: return editProdSuccess(state, action);
        case actionTypes.EDIT_PROD_FAIL: return editProdFail(state, action);

        case actionTypes.UPDATE_PROD_ID: return updateCurrentProductId(state, action);

    default: return state;
    }
};

export default reducer;