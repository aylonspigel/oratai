const ORATAI_CACHE = "simple-cache-v1";
const urlsToCache = ["/","/terms","/about","/contactus","/privacy"];

self.addEventListener("install", event => {
    const preLoaded = caches.open(ORATAI_CACHE)
        .then(cache => cache.addAll(urlsToCache))
    event.waitUntil(preLoaded);
});

self.addEventListener("fetch", event => {
    const response = caches.match(event.request)
        .then(match => match || fetch(event.request));
    event.respondWith(response);
});