upstream oratai_app {
  server 127.0.0.1:8080;
}

server {
    #listen 80 is default
    server_name www.testthisnow.ga;
    return 301 $scheme://testthisnow.ga$request_uri;
}


server {
    listen 80;
    listen   [::]:80;
    listen   443 default ssl;
    ssl on;

	# use the Let's ENcrypt certificates
    ssl_certificate /etc/letsencrypt/live/testthisnow.ga/fullchain.pem; # manag$
    ssl_certificate_key /etc/letsencrypt/live/testthisnow.ga/privkey.pem; # man$
        # include the SSL configuration from cipherli.st
    include /etc/nginx/snippets/ssl-params.conf;

    server_name testthisnow.ga;

    if ($ssl_protocol = "") {
       rewrite ^   https://$server_name$request_uri? permanent;
    }

    location / {
        proxy_set_header   X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   Host $http_host;
        proxy_set_header X-NginX-Proxy true;
        proxy_pass         https://oratai_app;
        proxy_redirect off;
    }
}
