const withCSS = require('@zeit/next-css')


module.exports = withCSS({
  webpack: (config) => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty'
    }
    
    return config
  },
  exportPathMap: function() {
        return {
          '/': { page: '/' },
          '/about': { page: '/about' },
          '/contactus': { page: '/contactus' },
          '/privacy': { page: '/privacy' },
          '/terms': { page: '/terms' },
          '/tips': { page: '/tips' },
          '/reviews': { page: '/reviews' },
          '/customers': { page: '/customers' }
        }
  }
})
