import React, { Component } from 'react';
import { connect } from 'react-redux';
import Router from 'next/router';

import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import Spinner from '../../components/UI/spinner/spinner';
import * as actions from '../../store/actions/index';
import { updateObject, checkValidity } from '../../shared/utility';

class Auth extends Component {
    state = {
        controls: {
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Mail Address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            }
        },
        isSignup: true
    }

    componentWillReceiveProps (prevState) {
        if ( this.props.isAuthenticated === 'true' ) {
            Router.push('/products');
            console.log('auth authenticated');
        }
    }
    // !this.props.buildingBurger &&
    componentDidMount () {
        if (  this.props.authRedirectPath !== '/' ) {
            this.props.onSetAuthRedirectPath();
        }
    }

    inputChangedHandler = ( event, controlName ) => {
        const updatedControls = updateObject( this.state.controls, {
            [controlName]: updateObject( this.state.controls[controlName], {
                value: event.target.value,
                valid: checkValidity( event.target.value, this.state.controls[controlName].validation ),
                touched: true
            } )
        } );
        this.setState( { controls: updatedControls } );
    }

    submitHandler = ( event ) => {
        event.preventDefault();
        this.props.onAuth( this.state.controls.email.value, this.state.controls.password.value, this.state.isSignup );
        this.props.onAuthClose();
        console.log('auth authenticated');
        Router.push('/products');
    }

    switchAuthModeHandler = (event) => {
        event.preventDefault();
        this.setState( prevState => {
            return { isSignup: !prevState.isSignup };
        } );
    }

    render () {
        const formElementsArray = [];
        for ( let key in this.state.controls ) {
            formElementsArray.push( {
                id: key,
                config: this.state.controls[key]
            } );
        }

        let form = formElementsArray.map( formElement => (
            <Input 
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                label={formElement.config.elementConfig.placeholder}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                inline='true'
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                autoC={formElement.config.elementConfig.type}
                changed={( event ) => this.inputChangedHandler( event, formElement.id )} />
        ) );

        if ( this.props.loading ) {
            form = <Spinner />
        }

        let errorMessage = null;

        if ( this.props.error ) {
            errorMessage = (
                <p>{this.props.error.message}</p>
            );
        }

        return (
            <div className='Auth'>
                {errorMessage}
                <form className="login-form" onSubmit={this.submitHandler}>
                    {form}
                    <div className="btn-div">
                        <Button btnType="Success inline">
                            {this.state.isSignup ? 'SIGNUP' : 'SIGNIN'}
                        </Button>
                    
                        <Button
                            clicked={this.switchAuthModeHandler}
                            btnType="Danger inline right">SWITCH TO {this.state.isSignup ? 'SIGNIN' : 'SIGNUP'}
                        </Button>
                    </div>
                </form>
            <style jsx>{`
            .Auth {
                position: relative;
                margin: 0px auto;
                width: 100%;
                display: block;
                text-align: center;
                border: 0px;
                padding: 10px;
                box-sizing: border-box;

            }
            .login-form{
                position: relative;
            }
            .btn-div{
                width: 100%;
                text-align: center;
            }
            .inline{
                display: inline-block;
            }
            
            @media (min-width: 600px) {
                .Auth {
                    width: 360px;
                    
                }
            }

        
            `}</style>
        </div>
        );
    }
}

// buildingBurger: state.burgerBuilder.building,
const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuthenticated: state.auth.token !== null,
        authRedirectPath: state.auth.authRedirectPath,
        authShow: state.auth.authShow,
        isAdmin: state.auth.isAdmin
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAuthClose: () => dispatch (actions.authClose()),
        onAuth: ( email, password, isSignup ) => dispatch( actions.auth( email, password, isSignup ) ),
        onSetAuthRedirectPath: () => dispatch( actions.setAuthRedirectPath( '/products' ) )
    };
};

export default connect( mapStateToProps, mapDispatchToProps )( Auth );